###
# Python 2/3 compatibility stuff
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from io import IOBase, open   #needed for python 3 file instance checking
#needed for string instance checking
try: basestring
except NameError: 
    basestring = (str, bytes)
    
from .Station import Station
from .common import string
from .constants import *

#basic python libraries
import struct, datetime, json, time
import numpy as np

print ('loading beta LTG 2')

class LtgCatalogHeader( ):
    '''
    The class is will create an object for use with the LTG catalogs, so that
    things are named in a human readable way
    '''
    def __init__( self, size, magic, version, stationID, epoch ):
        self.size = size
        self.magic = magic
        self.version = version
        #stupid python 3 unicode strings bs
        self.stationID = string( stationID )
        self.epoch = epoch

class LtgFile( ):
    '''
    This class method will create a LtgFile object to do operations on the files

    Attributes
    ----------
    open
    close
    make_catalog
    read
    '''

    def __init__(self, filelike, makeCatalog = True, ltg2=None):
        '''
        Constructor method to initialie the object

        Arguments
        ---------
        filelike : Python File object
        makeCtalog : boolean, instructing to create a make catalog object
        '''
        self.ltg2 = ltg2
        self.open(filelike) 

        self.catalog = {}
        self.fileLocationsByStationID = {}
        self.fileLocationsByEpoch = {}   
        self.fileLocationByArrivalEpoch = {}
        #flag, True for ltg2 files, false for legacy LTG files
        
        if makeCatalog:
            self.make_catalog()  
    
    def open( self, filelike ):
        '''
        This class is will open the input file for reading

        Arguments
        ---------
        filelike: Python file object

        '''
        #this is complicated by python2/3 compatibility 
        if isinstance( filelike, str ) or isinstance( filelike, basestring ):
            #it's a string, try and open
            self.f = open( filelike, 'rb' )
        elif isinstance( filelike, IOBase ):
            #then it's a file
            self.f = filelike
        else:
            raise Exception('%s does not seem to be a file'%repr(filelike))

        #guess the file type (ltg2 or ltg)
        if self.ltg2 is None:
            self.f.seek( 0 )
            v, = struct.unpack( 'H', self.f.read(2) )
            if v == 1:
                self.ltg2 = True
            else:
                self.ltg2 = False
        self.f.seek(0)
    
    def close( self ):
        self.f.close()

    def make_catalog( self ):
        '''
        This class method scans through the file, reading the header of each packet.
        Stores critical information on the packets in three dictionaries, 
        one keyed by fileLocation : catalog, 
        one keyed by stationID : fileLocationsByStationID
        one keyed by epoc time : fileLocationsByTime
        '''

        #the file pointer should already exist
        f = self.f

        ###
        # the first 4 bytes in the file are  the total number of packets in the file
        if self.ltg2:
            self.fileVersion,   = struct.unpack('H', f.read(2))
            headerSize,         = struct.unpack('H', f.read(2))
            totalNumberPackets, = struct.unpack('i', f.read(4))
        else:
            totalNumberPackets, = struct.unpack('i', f.read(4))
            self.fileVersion = 0
        #we'll want to figure out what the smallest time of the file is while we're in here
        baseTime   = 2**32  #should be a big enough epoc
        for i in range( totalNumberPackets ):
            #where are we now?
            fileLocation = f.tell()
            # read in the critial information, these values are always present
            # try:
            ###
            # note, the size number here is little endian, unlike 
            # the rest of the message!  
            if self.ltg2:
                try:
                    arrivalEpoch, size, magic, messageType, messageVersionMajor, messageVersionMinor = struct.unpack( 'IHBBBB', f.read(10) )
                except:
                    # Hit some bad message, skip
                    continue
            else:
                try:
                    size, magic, messageType, messageVersionMajor, messageVersionMinor = struct.unpack( 'HBBBB', f.read(6) )
                except:
                    # Hit some bad message, skip
                    continue

            # except:
            #     #somethings gone wrong, probably EOF
            #     break
            version = [messageType, messageVersionMajor, messageVersionMinor]


            # in almost all cases, we can also decode the time and station information as well
            # so, we'll catch the cases where that's not true
            if (version == [1,9,0]) or (version[0] != 1):
                #we can still decode the stationID, but nothing more
                try:
                    stationID = string( f.read( 10 ).strip() )
                except UnicodeDecodeError:
                    # Ran into a bad LTG message, skip
                    continue 
                ###
                # usually these files are the GPS data that the sensors send back, 
                # there might maybe be 1 or 2 stations with very very old firmware versions
                # it's probably safe to just skip them all, if not, uncomment below
                self.catalog[fileLocation] = LtgCatalogHeader( size, magic, version, stationID, None )
                if not stationID in self.fileLocationsByStationID:
                    self.fileLocationsByStationID[stationID] = []
                self.fileLocationsByStationID[stationID].append( fileLocation )
                #can't do the fileLocationsByTime, since there's no time
                if version == [4,9,0]:
                    # ltgPacket = self.read(fileLocation)
                    #this is a GPS packet, we can get time if we decode it
                    # try:
                    ltgPacket = self.read(fileLocation)
                    epoch = ltgPacket.epoch
                    if not epoch in self.fileLocationsByEpoch:
                        self.fileLocationsByEpoch[epoch] = []
                    self.fileLocationsByEpoch[epoch].append( fileLocation )

                    if not self.ltg2:
                        #assume a short 1 second delay before the packet hits the server
                        arrivalEpoch = epoch + 1
                    if not arrivalEpoch in self.fileLocationByArrivalEpoch:
                        self.fileLocationByArrivalEpoch[arrivalEpoch] = []
                    self.fileLocationByArrivalEpoch[arrivalEpoch].append( fileLocation )

                #skip to the next message
                f.seek( fileLocation )
                if self.ltg2:
                    f.seek( size+6, 1 )  #the -14 offsets for the portion we already read
                else:
                    f.seek( size+2, 1 )  #the -14 offsets for the portion we already read
                continue
            
            #in all other cases, we can do a little bit extra decoding of use
            try:
                stationID       = string( f.read( 10 ).strip() ) #these are byte strings
                epoch,          = struct.unpack( '>I', f.read(4) )
            except UnicodeDecodeError:
                # Ran into a bad LTG message, skip
                continue 

            #keep up on the baseTime
            if epoch < baseTime:
                baseTime = epoch
            
            #update the dictionaries
            self.catalog[fileLocation] = LtgCatalogHeader( size, magic, version, stationID, epoch )
            if not stationID in self.fileLocationsByStationID:
                self.fileLocationsByStationID[stationID] = []
            self.fileLocationsByStationID[stationID].append( fileLocation )

            if not epoch in self.fileLocationsByEpoch:
                self.fileLocationsByEpoch[epoch] = []
            self.fileLocationsByEpoch[epoch].append( fileLocation )
            
            if not self.ltg2:
                #assume a short 1 second delay before the packet hits the server
                arrivalEpoch = epoch + 1
            if not arrivalEpoch in self.fileLocationByArrivalEpoch:
                self.fileLocationByArrivalEpoch[arrivalEpoch] = []
            self.fileLocationByArrivalEpoch[arrivalEpoch].append( fileLocation )

            f.seek( size-18, 1 )    #the -18 offsets for the portion we already read

        #set the basetime
        self.baseTime = baseTime
 
    def read( self, fileLocation, decode=True, decompress=True ):
        '''
        This class takes a file location, reads the string and decodes it as an LtgPacket

        Arguments
        ---------
        fileLocation :int

        Returns
        -------
        Get the contents from file from the location.
        '''
        
        ###
        # loads the file specified by the catalog key 
        self.f.seek(fileLocation)
        #get the size of the packet
        if self.ltg2:
            arrivalEpoch, = struct.unpack( 'I', self.f.read(4) )
        size, = struct.unpack( 'H', self.f.read(2) )
          
        messageStr = self.f.read( size )
        return LtgPacket( messageStr, decode=decode, decompress=decompress )

    def write( self, filelike, keys, format='ltg2' ):
        pass

class LtgPacket( ):
    '''
    This class will create a Ltg packet.

    Attributes
    ----------
    decode
    find_peaks
    shift_epoch
    prepend
    _decodeGps
    _decode191
    _decode194
    _decompress194
    _decode1101

    '''
    def __init__(self, messageStr, lat=None, lon=None, decode=True, decompress=True ):
        
        self.messageStr = messageStr

        #get version information
        magic, messageType, versionMajor, versionMinor = struct.unpack( 'BBBB', messageStr[:4] )
        self.magic   = magic
        self.version = [messageType, versionMajor, versionMinor]
        self.size    = len(messageStr)
        
        #initialize some the of variable we check.  There are not always present, 
        #but frequently get checked for.  Putting in default values is convenient
        self.hf      = None
        self.hfPeaks = None
        self.lf      = None
        self.toga    = None 

        self.hfThreshold = 10000    #very very high

        #these guys might be None as well, we will set them later
        self.lat = lat
        self.lon = lon
        self.alt = 0
        self.gpsVisibility = True

        ###
        # decode the message header
        # I'm not sure why you'd ever not want to do this
        if decode:
            self.decode( decompress=decompress )


    def find_peaks( self ):
        '''
        This method is used for identifying the peaks from the Ltg packets

        Additional Information
        ----------------------
        This started as a simple peak finder,
        It's not so simple any more.
        There are 2 peak sets that it finds,
        peak - is a low rate of peaks,
        hfPeaks- is a much higher rate of peaks
        both are Nx2 arrays of time, amplitude values
        '''

        #do we have HF data to work with?
        if self.hf is None:
            return
        #get the length of the HF array
        N = len( self.hf )
        if N < 3:
            #less than 3 samples means no maxima/minima
            return

        #initialization
        hfPeaks     = []
        lastMin     = self.hf[0]
        lastMax     = self.hf[0]
        lastPeak    = [-1000000, 0]
        hfAmplitude = 0

        #loop over samples
        for i in range( 1, N ):
            #going up?
            if self.hf[i,1] > lastMax[1]:
                lastMax = self.hf[i]
            #going down?
            if self.hf[i,1] < lastMin[1]:
                lastMin = self.hf[i]
            #continuity
            if self.hf[i,0] - lastPeak[0] < DECIMATED_PEAKS_CONTINUITY:
                #update the timestamp
                lastPeak[0] = self.hf[i,0].copy()

            #update the amplitude
            if abs(lastMax[1]) > hfAmplitude:
                hfAmplitude = abs(lastMax[1])
            if abs(lastMin[1]) > hfAmplitude:
                hfAmplitude = abs(lastMin[1])

            #is the last maximum a peak?
            if lastMax[1] - self.hf[i,1] > PEAKS_THRESHOLD_FACTOR*self.hfThreshold and lastMax[1]-lastPeak[1] > PEAKS_THRESHOLD_FACTOR*self.hfThreshold:
                #apparently we're going down and it was!
                hfPeaks.append( lastMax )

                lastPeak = lastMax.copy()
                lastMin  = self.hf[i] 
                lastMax  = self.hf[i]

            #is the last minimum a peak?
            elif self.hf[i,1] - lastMin[1] > PEAKS_THRESHOLD_FACTOR*self.hfThreshold and lastPeak[1] - lastMin[1] > PEAKS_THRESHOLD_FACTOR*self.hfThreshold:
                #apparently we're going down and it was!
                hfPeaks.append( lastMin )

                lastPeak = lastMin.copy()
                lastMin  = self.hf[i] 
                lastMax  = self.hf[i]

        #the numpy arrays used here allow me to slice the hf data in convenient ways.
        #the data isn't mutable (it won't change), using a normal 2d array also makes sense 
        if len( hfPeaks ) > 0:
            self.hfPeaks = np.array( hfPeaks )
        else:
            self.hfPeaks = None
        self.hfAmplitude = hfAmplitude

    def decode( self, decompress=True ):
        '''
        This method is used for decoding the Ltg packet from the LTG file.
        '''
        if self.version[0] == 4:
            self._decodeGps(  )
        elif self.version == [1,9,1]:
            self._decode191( decompress=decompress )
        elif self.version == [1,9,4]:
            self._decode194( decompress=decompress )
        elif self.version == [1,10,1]:
            self._decode1101( decompress=decompress )
        #1.10.1 and 1.10.2 are fully compatable, one has LF the other does not
        elif self.version == [1,10,2]:
            self._decode1101( decompress=decompress )
        else:
            #I don't know what this is
            raise ValueError( "Unknown Version for Packet %s"%(repr(self.version)) )

    def _decodeGps(self, decode=False):
        '''
        This method is used for decoding the GPS from the LTG packet'

        Additional Information
        ----------------------
        This was ported from the production processor code, modified slightly to
        fit the format used here

        When we decode a location, we automatically update our buffer of sensor locations

        '''

        rawDataIndex = 0
        
        if self.version[1] == 9:
            rawDataIndex = 14
            nTotalLen = 246
        #it doesn't look like any packets with 4.8.x version exist.  I'm not sure if this decoding logic here will work properly
        elif self.version[1] == 8:
            rawDataIndex = 4
            nTotalLen = 4 + 154 + 78
        else:
            return False
        self.stationID = string ( (self.messageStr[4:14]).strip() )
        
        lengthTobeExtracted = nTotalLen - rawDataIndex
        S = self.messageStr[14:lengthTobeExtracted]
        rawDataIndex = 4
        self.GPSmonth,self.GPSday = struct.unpack( 'BB', S[rawDataIndex:rawDataIndex+2])
        rawDataIndex += 2
        self.GPSyear = struct.unpack( '>H', S[rawDataIndex: rawDataIndex+2 ])[0] #IPAddress.NetworkToHostOrder((short)ByteArrayToInt(b2b, 2));
        rawDataIndex += 2
        self.GPShour,self.GPSminute,self.GPSsecond = struct.unpack( 'BBB', S[rawDataIndex:rawDataIndex+3])
        
        dataTime = datetime.datetime(self.GPSyear, self.GPSmonth, self.GPSday, self.GPShour, self.GPSminute, self.GPSsecond)
        self.GPSPacketTimeinSeconds = (dataTime - datetime.datetime(1970,1,1)).total_seconds()
        
        self.epoch = self.GPSPacketTimeinSeconds

        rawDataIndex = 15
        nLat =  struct.unpack( '>i', S[rawDataIndex: rawDataIndex+4 ])[0]
        self.lat = self.gpsLatitude = (90.0 * nLat)/324000000
        
        rawDataIndex += 4
        nLon =  struct.unpack( '>i', S[rawDataIndex: rawDataIndex+4 ])[0]
        #*******************************
        #I'm a little worried this is wrong
        self.lon = self.gpsLongitude = (90.0 * nLon)/324000000

        #make sure they're in the right range
        if self.lon > 180:
            self.lon -= 360
        if self.lat > 90:
            self.lat -= 180
        
        rawDataIndex += 4
        nHeight =  struct.unpack( '>i', S[rawDataIndex: rawDataIndex+4 ])[0]
        self.alt  = (nHeight)/100.0
        
        rawDataIndex = 55
        self.GPSVisibleSatellites,self.GPSTrackedSatellites= struct.unpack( 'BB', S[rawDataIndex:rawDataIndex+2])

        #simple true false flag for if we're tracking satellites or not
        if self.GPSVisibleSatellites > 2 and self.GPSTrackedSatellites > 2:
            self.gpsVisibility = True
        else:
            self.gpsVisibility = False

        #update the location if it is valid
        #most likely issue is the sensor being reported to be at 'null island'
        #We have no sensors at 0,0, there is no island there.
        # if self.lat != 0 and self.lon !=0:
        station = Station( self.stationID, self.lat, self.lon, self.alt, self.gpsVisibility )
        return station
      
    def _decode191( self, decompress=True ):
        '''
        This class method is used to decode the 191 sensors

        Additional Information
        ----------------------
        the station ID is a string, so no need to use struct
        but it does tend to be too long
        '''
        S = self.messageStr
        self.stationID = string( (S[4:14]).strip() )
        
        #next are a bunch of numbers
        self.epoch,         = struct.unpack( '>I', S[14:18] )
        self.gpsOffset,     = struct.unpack( 'b',  S[18:19] )  #in ns
        self.hfSclkOffset,  = struct.unpack( 'B',  S[19:20] )   #sclk between pps an sample 0
        self.hfSclkPeriod,  = struct.unpack( 'B',  S[20:21] )   #should always be 5
        self.sclkCount,     = struct.unpack( '>I', S[21:25] )   #total sClk's in 1 second
        self.attenuation,   = struct.unpack( 'B',  S[25:26] )
        self.hfLength,      = struct.unpack( '>H', S[36:38] )
            
        #next we need to read the packages
        self.hfStr = S[38:38+self.hfLength]

        self.hf = None
        ###
        # next we need to decompress the hf and lf information
        if len( self.hfStr ) > 0:
            #decompress the data
            if decompress:
                hf = np.array( self._decompress191( self.hfStr ), dtype='int32' )

                #convert time to nanoseconds
                nsPerSclk = 1000000000.0/self.sclkCount #this will be 8 and change, and make everything floats
                #there's probably better ways to do this, but this should be accurate to 1 ns
                hf[:,0] = ((hf[:,0]*self.hfSclkPeriod - self.hfSclkOffset)*nsPerSclk).astype( 'int32' )
                self.hf = hf


    def _decompress191( self, S ):
        '''
        This class method is used to decompress the 191 sensors

        Additional Information
        ----------------------
        1.9.1 data is storred in a simple format (unlike 1.9.4 data)
        the messages are simply stored as 4byte time, 2byte amplitude
        '''

        #what is the length of the string
        N = int(len(S)/6)
        #the string does have an even number of samples, right?
        if len(S)%6 != 0:
            raise (Exception, 'Decode 1.9.1 length of string incorrect: %i'%len(S) )


        # there is probably a way to do this all at once, but for now
        # we'll loop through the file
        decompressed = []
        for i in range(N):
            d = struct.unpack( '>Lh', S[i*6:i*6+6])
            decompressed.append( d )
        return decompressed
    
    def _decode194(self, decompress=True):
        '''
        This class method is used to decode the 194 sensors
        '''
        # get the string input, 
        S = self.messageStr 
        self.stationID = string( (S[4:14]).strip() )
        
        #next are a bunch of numbers
        self.epoch,         = struct.unpack( '>I', S[14:18] )
        self.gpsOffset,     = struct.unpack( 'b',  S[18:19] )  #in ns
        self.hfSclkOffset,  = struct.unpack( 'B',  S[19:20] )   #sclk between pps an sample 0
        self.hfSclkPeriod,  = struct.unpack( 'B',  S[20:21] )   #should always be 5
        self.sclkCount,     = struct.unpack( '>I', S[21:25] )   #total sClk's in 1 second
        self.attenuation,   = struct.unpack( 'B',  S[25:26] )
        self.hfThreshold,   = struct.unpack( '>H', S[26:28] )
        self.lfSclkOffset,  = struct.unpack( 'B',  S[28:29] )   
        self.lfSclkPeriod,  = struct.unpack( 'B',  S[29:30] )   #should always be about 192
        self.lfThreshold,   = struct.unpack( '>H', S[30:32] )
        self.hfDcOffset,    = struct.unpack( '>h', S[32:34] )
        self.lfDcOffset,    = struct.unpack( '>h', S[34:36] )
        self.spSclkPeriod,  = struct.unpack( '>I', S[46:50] )
        self.thresholdTab,  = struct.unpack( 'B',  S[50:51] )   #0--old(4bit) 1--new(3bit)
        self.spType,        = struct.unpack( 'B',  S[51:52] )  #0--NoSpectra, 1--Low 2--High, 3--System10
        self.hfLength,      = struct.unpack( '>H', S[52:54] )
        self.lfLength,      = struct.unpack( '>H', S[54:56] )
        self.spLength,      = struct.unpack( '>H', S[56:58] )
            
        #next we need to read the packages
        self.hfStr = S[58:58+self.hfLength]
        self.lfStr = S[58+self.hfLength:58+self.hfLength+self.lfLength]
        self.spStr = S[58+self.hfLength+self.lfLength:58+self.hfLength+self.lfLength+self.spLength]

        ###
        # next we need to decompress the hf and lf information
        if len( self.hfStr ) > 0 and decompress:
            decompressed = self._decompress194( self.hfStr )

            #it's possible we got back no data.  Unlikely but possible
            if len( decompressed) > 0:
                hf = np.array( decompressed, dtype='int32' )

                #convert time to nanoseconds
                nsPerSclk = 1000000000.0/self.sclkCount #this will be 8 and change, and make everything floats
                #there's probably better ways to do this, but this should be accurate to 1 ns
                hf[:,0] = ((hf[:,0]*self.hfSclkPeriod - self.hfSclkOffset)*nsPerSclk).astype( 'int32' )
                self.hf = hf
                
        if len( self.lfStr ) > 0 and decompress:
            decompressed = self._decompress194( self.lfStr )
            
            if len( decompressed ) > 0:  
                lf = np.array(decompressed, dtype='int32' )

                #convert the time to nanoseconds
                nsPerSclk = 1000000000.0/self.sclkCount 
                lf[:,0] = ((lf[:,0]*self.lfSclkPeriod - self.lfSclkOffset)*nsPerSclk).astype('int32')
                self.lf = lf


    def _decompress194( self, S ):
        '''
        This class method is used for decompressed data is in time,amplitude pairs, with time in sclocks
        '''
        ###1.9.4 decompression, should be backwards compatible with 1.9.3 data
        
        directionState = 0  #0 for down, 1 for up
        
        decompressed = []
        
        i = 0
        while i < len(S):
            byteType = byte2type( S[i] )
            B, = struct.unpack( 'B', S[i:i+1] )
            #case statement (with if's)
            if byteType == 0:
                ###
                # Type 0 1<7> (1 byte)
                # A leading one, followed by a seven bit integer indicates progression
                # to the next amplitude threshold.  Direction is unchanged, so if we
                # were rising before, we continue rising to the next larger threshold
                # value. The seven bit integer indicates how many ticks have elapsed
                # between threshold crossings.
                # tick = tickOfLastPoint + 7bitInteger
                # amplitude = nextThreshold
                
                tim, = struct.unpack( 'B', S[i:i+1] )
                # cut off that leading 1 and add to previous state
                if len( decompressed ) > 0: #but, we need to have a previous sample
                    tim = (tim & 0x7f)+ decompressed[-1][0]
                    
                    if directionState == 0:
                        #going down
                        amp = boundary_prev( decompressed[-1][1] )
                    else:
                        #going up
                        amp = boundary_next( decompressed[-1][1] )
                    # decompressed.append( [tim, amp] )

                    #bound checking, time should be in order
                    if tim > decompressed[-1][0]:
                        decompressed.append( [tim, int(amp)] )
                i += 1
            elif byteType == 1:
                ###
                # Type 1 01<6><8> (2 bytes)
                # A leading '01', followed by a six bit integer and an eight bit integer
                # denotes a maximum (if rising) or a minimum (if falling).
                # The 8 bit integer is the number of ticks elapsed since the previous point.
                # The amplitude is between two thresholds. One is the next threshold, and
                # the other is the threshold beyond that (farther from 0). Call the smaller
                # of these a0 and the larger a1.
                # tick = tickOfLastPoint + 8bitInteger
                # amplitude = a0 + (6bitInteger/64) * (a1 - a0)
                if i+2 > len(S):
                    break
                amp, tim = struct.unpack( 'BB', S[i:i+2] )
                if len(decompressed) > 0:
                    tim = tim + decompressed[-1][0]
                
                    ###
                    # going up or down?
                    if directionState == 0:
                        #going down
                        a1 = boundary_prev( decompressed[-1][1] )
                        a0 = boundary_prev( a1 )
                    else:           
                        #going up
                        a0 = boundary_next( decompressed[-1][1] )
                        a1 = boundary_next( a0 )
                    # chop off leading 01, and interpolate
                    amp = a0 + ((amp & 0x3f)*(a1 - a0))/64
                    #peakInfo.append([tim,amp])
                    
                    #bound checking, time should be in order
                    if tim > decompressed[-1][0]:
                        decompressed.append( [tim, int(amp)] )
                    directionState = (directionState+1)%2   #flip direction state       
                i += 2
            elif byteType == 2:
                ###
                # Tyoe 2     001<5><16> (3 bytes)
                # A leading '001' followed by a five bit integer and a sixteen bit integer
                # denotes a maximum or minimum.
                # The 16 bit integer is the number of ticks elapsed since the previous point.
                # The 5 bit integer gives the amplitude of the peak.
                # 
                # tick = tickOfLastPoint + 16bitInteger
                # amplitude = a0 + (5bitInteger/32) * (a1 - a0)
                if i+3 > len(S):
                    break
                amp, tim = struct.unpack( '>BH', S[i:i+3] )
                
                if len(decompressed) > 0:
                    tim = tim + decompressed[-1][0]
                    ###
                    # going up or down?
                    if directionState == 0:
                        #going down
                        a1 = boundary_prev( decompressed[-1][1] )
                        a0 = boundary_prev( a1 )
                    else:           
                        #going up
                        a0 = boundary_next( decompressed[-1][1] )
                        a1 = boundary_next( a0 )
                    # chop off leading 001, and interpolate
                    amp = a0 + ((amp & 0x1f)*(a1 - a0))/32
                    #peakInfo.append([tim,amp])
                    
                    #bound checking, time should be in order
                    if tim > decompressed[-1][0]:
                        decompressed.append( [tim, int(amp)] )
                    directionState = (directionState+1)%2   #flip direction state
                i += 3

            elif byteType == 3:
                ###
                # Type 3  0001<12> (2 bytes)
                # A leading '0001', followed by a twelve bit integer indicates progression
                # to the next amplitude threshold.  Direction is unchanged, so if we
                # were rising before, we continue rising to the next larger threshold
                # value. The twelve bit integer indicates how many ticks have elapsed
                # between threshold crossings.
                #
                # tick = tickOfLastPoint + 12bitInteger
                # amplitude = nextThreshold
                if i+2 > len(S):
                    break
                tim, = struct.unpack( '>H', S[i:i+2] )
                
                if len(decompressed) > 0:
                    tim = (tim & 0x0fff)+ decompressed[-1][0]

                    if directionState == 0:
                        #going down
                        amp = boundary_prev( decompressed[-1][1] )
                    else:
                        #going up
                        amp = boundary_next( decompressed[-1][1] )

                    #Bounds checking
                    if tim > decompressed[-1][0]:
                        decompressed.append( [tim, int(amp)] )
                
                i += 2

            elif byteType == 4:
                ###
                # Type 4  00001<11> (2 bytes)
                # A leading '00001', followed by an eleven bit integer indicates progression
                # through 0 to an amplitude equal to the amplitude of the last point negated.
                #
                # tick = tickOfLastPoint + 11bitInteger
                # amplitude = -lastAmplitude
                if i+2 > len(S):
                    break
                    
                tim, = struct.unpack( '>H', S[i:i+2] )
                
                if len(decompressed) >0:
                    tim = (tim & 0x07ff)+ decompressed[-1][0]
                    amp = -decompressed[-1][1]
                

                    if tim > decompressed[-1][0]:
                        decompressed.append( [tim, int(amp)] )
                i += 2      
            elif byteType == 5:
                ###
                # Type 5 000001<2><16> (3 bytes)
                # Five leading zeros indicate an arbitrary amplitude 1 to 5 ticks after
                # the preceding point.  The first 2 bits indicate 1 less than the number
                # of ticks from the previous point, and the next 16 bits are a signed
                # amplitude. 
                if i+3 > len(S):
                    break
                tim, amp = struct.unpack( '>Bh', S[i:i+3] )
                
                #mask out the time (6 bits)
                if len( decompressed ) > 0:
                    tim = decompressed[-1][0] + (tim & 0x03) + 1
                else:
                    tim = (tim & 0x03) + 1

                #bound checking, time should be in order
                if len(decompressed) > 0:
                    if tim > decompressed[-1][0]:
                        decompressed.append( [tim, int(amp)] )
                else:
                    decompressed.append( [tim, amp] )
                
                i += 3
            elif byteType == 6:
                ###
                # Type 6 0000001<1> (1 byte)
                # A leading '0000001' followed by a one bit integer indicates whether we
                # are going up or down. '00000010' means "up", '00000011' means "down".
                # There are no tick and amplitude associated with a type 6 message, it
                # just tells how to interpret the following type 0-4 message.
                # direction = 1 bit integer         
                
                if B == 2:
                    directionState = 1
                elif B == 3:
                    directionState = 0
                else: 
                    #somethings gone terribly wrong
                    #should I raise an error?
                    break
                i += 1
            elif byteType == 7: 
                ###
                # Type 7 0000000<25><16> (6 bytes)
                # Seven leading zeros indicate the traditional 6 byte tuple. This works
                # because the largest tick number that can be generated by our ADC is
                # (a bit more than) 24000000, which fits in 25 bits. This means that
                # our old uncompressed 1.9.3 message bodies are also valid 1.9.4 message
                # bodies, in which all points are encoded as type 7. This message type is
                # used for points that we can't compress into any of the other types, 
                # because the jump in ticks or amplitude is too large to fit into the
                # small number of bytes available in those types. The first message and
                # last point of each second are always of this type.            
                if i+6 > len(S):
                    break
                tim, amp = struct.unpack( '>Ih', S[i:i+6] )
                #bound checking, time should be in order
                if len(decompressed) > 0:
                    if tim > decompressed[-1][0]:
                        decompressed.append( [tim, int(amp)] )
                else:
                    decompressed.append( [tim, amp] )
                
                
                i += 6
            else:
                #something went terribly wrong

                raise Exception('ERROR - LTGReader encountered unexected data compression')
                return None
       
        return decompressed


    def _decode1101(self, decompress=True):
        '''
        The class method is used to decode the message. the station ID is a string, so no need to use struct
        but it does tend to be too long
        '''

        S = self.messageStr
        self.stationID = string( (S[4:14]).strip() )

        #next are a bunch of numbers
        self.epoch,         = struct.unpack( '>I', S[14:18] )
        self.gpsOffset,     = struct.unpack( 'b',  S[18:19] )  #in ns
        self.totalSize,     = struct.unpack( '>H',  S[19:21] )
        self.sclkCount,     = struct.unpack( '>I', S[21:25] )   #total sClk's in 1 second
        self.attenuation,   = struct.unpack( 'B',  S[25:26] )   #in dB
        self.timeFirmware,  = struct.unpack( '>I', S[26:30] )
        
        self.lf = None
        self.hf = None
        #what follows from this point are submessages, each with their 
        #own formatting.  
        i0 = 30
        delayed = []    #we delay decoding these strings
        while i0+3 <= self.size:
            #these sizes are in the message sent by the lightning sensor, 
            #so the size is in network byte order, not little endian
            subSize, subMagic = struct.unpack( '>HB', S[i0:i0+3] )
            if subSize==0:
                raise ValueError(f"subSize is ZERO")
            if subMagic == 49:
                #0x31 - HF data
                try:
                    self.__decode1101HF( S[i0:i0+subSize], decompress=decompress )
                except IndexError:
                    pass
            elif subMagic == 50:
                #0x32 - LF data
                try:
                    self.__decode1101LF( S[i0:i0+subSize], decompress=decompress )
                except IndexError:
                    pass
            elif subMagic == 51:
                #0x33 - GPS data
                self.__decode1101GPS( S[i0:i0+subSize] )
            elif subMagic == 52:
                #0x34 - Spectra
                self.__decode1101Spec( S[i0:i0+subSize] )
            elif subMagic == 53:
                #0x35 - Text Message
                self.__decode1101Text( S[i0:i0+subSize] )
            elif subMagic == 54:
                #0x36 - TOGA
                #this is based on LF data
                if self.lf is None:
                    delayed.append( S[i0:i0+subSize] )
                    i0 += subSize
                    continue
                self.__decode1101Toga( S[i0:i0+subSize] )
            elif subMagic == 55:
                #0x37 - ELF
                #this is based on LF data
                if self.lf is None:
                    delayed.append( S[i0:i0+subSize] )
                    i0 += subSize
                    continue
                self.__decode1101ELF( S[i0:i0+subSize] )
            else:
                #what went wrong here?
                raise Exception( 'ERROR: unknown submagic: %i'%subMagic )

            i0 += subSize
        
        #loop over delayed strings
        for S in delayed:
            # we delayed decoding because we needed an LF string to get 
            # the timing information.  But, there's a chance we still 
            # don't have an LF string.  In that case, I guess we won't 
            # do decoding.
            if self.lf is None:
                continue
            i0 = 0
            subSize, subMagic = struct.unpack( '>HB', S[i0:i0+3] )
            if subSize==0:
                raise ValueError(f"subSize is ZERO")
            if subMagic == 54:
                #0x36 - TOGA
                #this is based on LF data
                self.__decode1101Toga( S[i0:i0+subSize] )
            elif subMagic == 55:
                #0x37 - ELF
                #this is based on LF data
                self.__decode1101ELF( S )
        
    def __decode1101HF( self, S, decompress=True ):
        '''
        The class method is used to decode HF

        Arguments
        ---------
        :param S: Content from the Ltg file
        :return:
        '''
        size, magic = struct.unpack( '>HB', S[:3] )
        
        ###
        # idiot checking
        if magic != 49:
            #this isn't HF data!
            raise ValueError

        self.hfSclkOffset,  = struct.unpack( 'B',  S[3:4] ) #sclk between pps an sample 0
        self.hfThreshold,   = struct.unpack( '>H', S[4:6] )
        self.hfDcOffset,    = struct.unpack( '>h', S[6:8] )
        ###
        #There are 3 numbers I'm not sure of the meaning for
        self.hfBlkOffset,   = struct.unpack( '>I', b'\x00'+S[8:11] )    #3 byte number!
        self.hfBlkCount,    = struct.unpack( '>H', S[11:13] )
        self.hfBlkStart,    = struct.unpack( '>I', b'\x00'+S[13:16] )#3 byte number!
        #note, hfSclkPeriod is no longer stored, because it's always 5
        self.hfSclkPeriod = 5

        ###
        # the remainder of the string is 1.9.4 compressed waveshape
        self.hfStr = S[16:]
        if len(self.hfStr) < 1:
            #then there's no HF string
            self.hf = None
            return
        
        #decompress the HF string
        #we convert things to an np.array here, for math convenience 
        if decompress:
            hf = np.array( self._decompress194( self.hfStr ), dtype='int32' )

            #convert time to nanoseconds
            nsPerSclk = 1000000000.0/self.sclkCount #this will be 8 and change, and make everything floats
            #there's probably better ways to do this, but this should be accurate to 1 ns
            hf[:,0] = ((hf[:,0]*self.hfSclkPeriod - self.hfSclkOffset)*nsPerSclk).astype( 'int32' )

            self.hf = hf

    def __decode1101LF( self, S, decompress=True ):
        '''
        The class method is used to decode LF

        Arguments
        ---------
        :param S: Content from the Ltg file
        :return:
        '''
        size, magic = struct.unpack( '>HB', S[:3] )
        
        ###
        # idiot checking
        if magic != 50:
            #this isn't HF data!
            raise ValueError('This is not an LF String {}'.format(magic))

        self.lfSclkOffset,  = struct.unpack( 'B',  S[3:4] ) #sclk between pps an sample 0
        self.lfThreshold,   = struct.unpack( '>H', S[4:6] )
        self.lfDcOffset,    = struct.unpack( '>h', S[6:8] )
        ###
        # filter stuff
        self.lfFiltFreq,    = struct.unpack( 'B', S[8:9] )  #should be 50 or 60
        self.lfFiltAmp,     = struct.unpack( '>H', S[9:11] )
        self.lfFiltOffset,  = struct.unpack( '>H', S[11:13] )   #between the maximum of the filter and PPS
        #note, lfSclkPeriod is no longer stored, because it's always 192
        self.lfSclkPeriod = 192
 
        ###
        # the remainder of the string is 1.9.4 compressed waveshape
        self.lf = []
        self.lfStr = S[13:]
        if len(S) <= 13:
            #then there's no LF string
            return
        
        if decompress:
            decompressed = self._decompress194( self.lfStr  )
            
            lf = np.array(decompressed, dtype='int32' )
            #convert time to seconds
            nsPerSclk = 1000000000.0/self.sclkCount #this will be 8 and change, and make everything floats
            lf[:,0] = ((lf[:,0]*self.lfSclkPeriod - self.lfSclkOffset)*nsPerSclk).astype( 'int32' )
            self.lf = lf
            
    def __decode1101GPS( self, S ):
        '''
        The class method is used to decode GPS

        Arguments
        ---------
        :param S: Content from the Ltg file
        :return:
        '''
        # When we decode a location, we automatically update our buffer of sensor locations

        size, magic = struct.unpack( '>HB', S[:3] )
        
        ###
        # idiot checking
        if magic != 51:
            #this isn't HF data!
            raise ValueError('This is not an GPS String {}'.format(magic))      
        
        self.gpsTracked,    = struct.unpack( 'B',  S[3:4] ) #num satellites tracked (hopefully not 0) 
        self.gpsVisible,    = struct.unpack( 'B',  S[4:5] ) #num satellites visible
        self.gpsLatitude,   = struct.unpack( '>i', b'\x00'+S[5:8] ) #3byte number
        self.gpsLongitude,  = struct.unpack( '>i', b'\x00'+S[8:11] )    #3byte number
        if self.gpsTracked > 2 and self.gpsVisible > 2:
            self.gpsVisibility = True
        else:
            self.gpsVisibility = False
        
        #correct the latitude and longitude
        #note, altitude isn't in here, so we don't set it.  That's ok, we don't really 
        #need the altitude of the sensor for location.  
        self.lat  = self.gpsLatitude*90./2**23
        self.lon  = self.gpsLongitude*180./2**23
        #I didn't convert these before, but I really should do
        self.gpsLatitude  = self.gpsLatitude*90./2**23
        self.gpsLongitude = self.gpsLongitude*180./2**23
        if self.lon > 180:
            self.lon -= 360
        if self.lat > 90:
            self.lat -= 180


    def __decode1101Spec( self, S ):
        '''
        Sectra decoding is only partially implemented.  the spectra is read in,
        but not actually decoded in a format that is useful (like freq vs. power).
        How it's stored is also funny, so this isn't so trival to do.

        Not really sure what Stan did here for encoding the data
        '''
        size, magic = struct.unpack( '>HB', S[:3] )
        
        ###
        # idiot checking
        if magic != 52:
            #this isn't Spec data!
            raise ValueError('This is not an Spec String {}'.format(magic))
        
        self.spSclkPeriod = struct.unpack( '>I', b'\x00'+S[3:6] )
        
        #the string
        self.spStr = S[6:]
        
        #decode the spectra
        self.sp = struct.unpack( '>512H', S[6:] )
        
    def __decode1101Text( self, S ):
        size, magic = struct.unpack( '>HB', S[:3] )
        
        ###
        # idiot checking
        if magic != 53:
            #wrong message format
            raise ValueError    
        
        #this one is pretty easy...
        self.text = S[3:]
        
    def __decode1101Toga( self, S ):
        '''
        IF multiple Toga's are present in a second, the this message 
        will be repeated.  
        The 8bit sample amplituede are constructed from the 16bit input as:
        m = max( abs(16bit waveform amplitudes) )
        8bit waveform = ((((16bit waveform)<<8) +1 )/m)>>1
        
        The 16bit amplitudes can be approximately reconstructed using 
        *m>>7
        the approximation is exact if m<128
        
        The documentation *does not* specify the data sampling rate!  
        This is what I've reconstructed with converstations with 
        James Brundel and Stan Heckman:
        The data rate is 1 sample every 3072 sclks, which is 1 sample 
        every 16 LF samples which seems plausible.  The data is aslo 
        put through a narrow band filter, to match other WWLLN sensors.  
        This filter adds a 81972 sclk sample delay to the waveform.  
        All of these issues are corrected in this module.
        '''
        size, magic = struct.unpack( '>HB', S[:3] )
        
        #have we already seen a Toga?
        if self.toga is None:
            #no, we haven't
            self.toga = []
            self.togaSclkOffset = []
            self.togaMaxAmplitude = []
        
        ###
        # idiot checking
        if magic != 54:
            #this isn't HF data!
            raise ValueError('This is not an TOGA String {}'.format(magic))
        
        
        self.togaSclkOffset.append(   struct.unpack( '>i', S[3:7] )[0] )
        self.togaMaxAmplitude.append( struct.unpack( '>h', S[7:9] )[0] )
        #~ lf[:,0] = ((lf[:,0]*self.lfSclkPeriod - self.lfSclkOffset))/float(self.sclkCount) + self.gpsOffset*40e-9
        #then the toga itself
        d = np.zeros( [64,2] )
        d[:,1]  = struct.unpack( '>64b', S[9:] )
        d[:,1] *= abs(self.togaMaxAmplitude[-1])>>7
        #~ d[:,0] = ((np.arange(64)*self.hfSclkPeriod*2**8)+self.togaSclkOffset[-1])/float(self.sclkCount) + self.gpsOffset*40e-9
        d[:,0] = ((np.arange(64)*3072)+self.togaSclkOffset[-1] - 81792)/float(self.sclkCount) #+ self.gpsOffset*40e-9
        self.toga.append( d )
        
    def __decode1101ELF( self, S ):
        '''
        Documentation for the ELF data is lacking, and I'm not 100% sure 
        that it will end up being useful.  The ELF data is generated by 
        running the LF waveform (before decimation) through an LPF and 
        summation.  There is 1 ELF sample every 2048 samples of LF data, 
        yielding 305-306 samples every second.  
        '''
        size, magic = struct.unpack( '>HB', S[:3] )
        ###
        # idiot checking
        if magic != 55:
            #this isn't HF data!
            raise ValueError('This is not an ELF String {}'.format(magic))
        
        self.elfClkOffset = struct.unpack( '>H', S[3:5] )
        
        ###
        # the rest of the message is samples,
        # the data is stored in 2 byte samples, but there can be a variable 
        # number of them.  So, we'll get the number of samples from the 
        # size of the file
        N = int((size-5)/2)

        if len( S[5:] ) != 2*N:
            #something has gone horribly wrong!
            #the size of the string doesn't seem to match the parameter in the header
            #maybe something got truncated?
            #ELF data is non-critical, so we're going to just not decode it
            self.elf = None
            return
        
        self.elf = np.zeros( [N,2] )            
        #these are the amplitudes
        self.elf[:,1] = struct.unpack( '>%ih'%N, S[5:] )
        #these are the times
        
        self.elf[:,0] = ((np.arange( N )*2048+self.elfClkOffset)*self.lfSclkPeriod+self.lfSclkOffset)/float(self.sclkCount) + self.gpsOffset*40e-9
            

class LtgPortion( ):
    """
    The portion terminology is pulled from the legacy process.
    A portion is the section of waveform which is associated with 1 pulse (or solution) for 1 station
    These are only used for classification, as this requires all of the waveform samples
    """

    def __init__( self, ltgPacket, expectedNano, peakCurrent, distance ):
        #note, the initialization a portion is different here than it is for lxlocate.  
        #this should be easier to work with
        self.ltgPacket    = ltgPacket
        self.epoch        = ltgPacket.epoch
        self.stationID    = ltgPacket.stationID
        self.expectedNano = expectedNano
        self.peakCurrent  = peakCurrent
        self.distance     = distance
        self.preTrigger   =  80000
        self.postTrigger  = 200000
        self.timeConstant =  40000
        self.peaksMaxMinContinuity = 20000
        self.peaksContinuity       = 4000
        self.peaksAmplitudeFactor  = 1.75

        #these will get set in a second
        #intialize them to None so we know if something's gone wrong
        self.samples          = None
        self.peaks            = None
        self.numPositivePeaks = None
        self.numNegativePeaks = None
        self.halfWidth        = None
        self.riseTime         = None
        self.fallTime         = None
        self.overshootTime    = None
        self.overshootRatio   = None
        self.normalizedAmplitude = None
        self.nextSampleNano   = None
        self.duration         = None

        #indices of importance
        self.primaryFeaturePeakIndex   = None
        self.primaryFeatureSampleIndex = None
        self.halfWidthSampleIndex      = None
        self.overshootPeakIndex        = None

        #do measurements
        self.get_samples()
        self.find_peaks()
        self.count_peaks()
        self.calc_halfwidth()
        self.calc_risetime()
        self.calc_falltime()
        self.calc_overshoot()
        self.calc_amplitude()
    
    def get_samples( self ):
        self.samples = None
        ### 
        # Idiot Checks
        if self.ltgPacket.hf is None:
            #there are so samples
            return
        if len( self.ltgPacket.hf ) == 0:
            #there are no samples
            return

        ####
        # get the samples
        # first, find the index of the sample nearest to the expectedNano
        dt = abs( self.ltgPacket.hf[:,0]-self.expectedNano )
        iExpected = dt.argmin()
        if dt[iExpected] > self.postTrigger and dt[iExpected] > self.preTrigger:
            #there are no samples to work with 
            # print ('no samples found')
            return

        # first find the starting sample
        iStart = iExpected
        while self.ltgPacket.hf[iStart,0]-self.ltgPacket.hf[iStart-1,0] < self.peaksMaxMinContinuity and self.ltgPacket.hf[iStart-1,0] > self.expectedNano - self.preTrigger:
            iStart -= 1
            if iStart <= 0:
                iStart = 0
                break

        #the find the last sample
        iStop = iExpected
        while self.ltgPacket.hf[iStop+1,0]-self.ltgPacket.hf[iStop,0] < self.peaksMaxMinContinuity and self.ltgPacket.hf[iStop+1,0] < self.expectedNano + self.postTrigger:
            iStop += 1
            if iStop >= len(self.ltgPacket.hf)-1:
                break

        if iStart == iStop:
            #there are no samples
            return

        self.samples = self.ltgPacket.hf[ iStart:iStop ]
        
        #store information about the next sample time, to allow computation of how isolated the pulse is
        if iStart > 0:
            previousNano = self.ltgPacket.hf[iStart-1,0]
        else: previousNano = 0  #start of the second, TODO make this the ltg prepend time instead
        if iStop < len( self.ltgPacket.hf ):
            nextNano = self.ltgPacket.hf[iStop, 0]
        else:
            nextNano = 1000000000   #1 second
        self.nextSampleNano = previousNano, nextNano
        self.duration = self.samples[-1,0] - self.samples[0,0]

    def find_peaks( self ):
        """
        finds all the peaks in the samples of the portion

        This is very similar, but not the same as find_peaks in ltgReader.LtgPacket
        Different enough that using that method may not be feasible 
        """

        if self.samples is None:
            self.peaks = None
            return

        timeConstant = self.timeConstant
        lastPeak     = (-6000000, 0)

        #The number of samples here might be very small, so we can't just ignore the first 
        #and last samples.  So we add synthetic ones
        samples = [[self.samples[0][0]-1,0]] + self.samples.tolist() + [[self.samples[-1][0]+1,0]]

        peaks = []
        #loop over the samples
        for i in range( 1, len(samples)-1 ):

            #what is the last amplitude
            if samples[i][0] - samples[i-1][0] > self.peaksMaxMinContinuity:
                lastAmp = 0
            else:
                lastAmp = samples[i-1][1]

            #what is the next amplitude?
            if samples[i+1][0] - samples[i][0]  < self.peaksMaxMinContinuity:
                nextAmp = samples[i+1][1]
            else:
                nextAmp = 0

            #this is a single point
            #this shouldn't happen in portions
            if nextAmp==0 and lastAmp==0:
                continue

            #is this a peak?
            peakThreshold = abs(lastPeak[1]) * np.exp( -(samples[i][0]-lastPeak[0])/timeConstant )
            if (samples[i][1] > 0 and lastAmp < samples[i][1] and nextAmp < samples[i][1]) or \
               (samples[i][1] < 0 and lastAmp > samples[i][1] and nextAmp > samples[i][1]):
                #this is a relative maxima or a relative minima
                #is the amplitude large enough to use
                if abs(samples[i][1]) > peakThreshold or ( abs(samples[i][1]-lastPeak[1] )) > peakThreshold:
                    #yes it is, keep the peak
                    peak = samples[i]

                    #because of noisy signals we can't just append the peak
                    #it's got to conform to some additional requirements
                    #if there's enough time between this peak and the last peak, keep it
                    if peak[0]-lastPeak[0] > self.peaksContinuity or len(peaks) == 0:
                        peaks.append( peak )
                        lastPeak = peak

                    #if the polarity of this peak and the previous peak are different, keep it
                    elif peak[1]*lastPeak[1] < 0:
                        peaks.append( peak )
                        lastPeak = peak

                    #the peaks are the same polarity, and close in time, maybe replace?
                    elif abs(peak[1]) > self.peaksAmplitudeFactor*abs(lastPeak[1]):
                        peaks[-1] = peak
                        lastPeak = peak

        if peaks != []:
            self.peaks = np.array(peaks)
        
        ###
        # find the index of the peak feature
        maxPeak   = 0
        peakIndex = None
        for i in range( len(peaks) ):
            t,a = peaks[i]
            #check polarity
            if np.sign( a ) != np.sign( self.peakCurrent):
                continue
            #check timing
            if abs( self.expectedNano - t ) > self.peaksMaxMinContinuity: 
                continue

            if abs(a) > maxPeak*1.5:
                peakIndex = i
                maxPeak = abs(a)

        self.primaryFeaturePeakIndex = peakIndex
        if peakIndex is None: return
        for sampleIndex in range( len(self.samples) ):
            if self.samples[sampleIndex,0] == peaks[ peakIndex ][0]:
                self.primaryFeatureSampleIndex = sampleIndex
                break

    def count_peaks( self ):
        """
        Counts the number of positive and negative peaks who's amplitude are close to the primary feature
        """
        if self.peaks is None or self.primaryFeaturePeakIndex is None:
            self.find_peaks()
        if self.peaks is None or self.primaryFeaturePeakIndex is None:
            #we can't do this without a primary feature
            return
        
        primaryAmplitude = abs( self.peaks[self.primaryFeaturePeakIndex][1] )

        numPositivePeaks = 0
        numNegativePeaks = 0
        for t,a in self.peaks:
            if a > 0.5*primaryAmplitude:
                numPositivePeaks += 1
            elif a < -0.5*primaryAmplitude:
                numNegativePeaks += 1
        
        if numPositivePeaks == 0 and numNegativePeaks == 0:
            #we didn't find any, something is wrong
            return
        
        self.numPositivePeaks = numPositivePeaks
        self.numNegativePeaks = numNegativePeaks

    def calc_halfwidth( self ):
        if self.primaryFeatureSampleIndex is None:
            self.find_peaks()
        if self.primaryFeatureSampleIndex is None:
            #we really need one of these for this calculation
            return
        
        #Start at the primaryFeatureIndex and count back until we drop below 50% amplitude, or hit the start
        primaryAmplitude = self.samples[self.primaryFeatureSampleIndex,1]
        threshold = 0.5 * abs( primaryAmplitude )
        
        iStart = self.primaryFeatureSampleIndex
        while abs( primaryAmplitude - self.samples[ iStart, 1 ] )  < threshold:
            if iStart <= 0:
                break
            #big jump check
            if self.samples[ iStart,0] - self.samples[ iStart-1,0] > self.peaksContinuity:
                break
            iStart -= 1
        #add interpolation?

        iStop = self.primaryFeatureSampleIndex
        while abs( primaryAmplitude - self.samples[ iStop, 1 ] ) < threshold:
            if iStop >= len( self.samples )-1:
                break
            #big jump check
            if self.samples[ iStart+1,0] - self.samples[ iStart,0] > self.peaksContinuity:
                break
            iStop += 1

        #add interpolation?

        self.halfWidthSampleIndex = [iStart,iStop]
        self.halfWidth = self.samples[iStop,0]-self.samples[iStart,0]

    def calc_risetime( self ):
        if self.primaryFeatureSampleIndex is None:
            self.find_peaks()
        if self.primaryFeatureSampleIndex is None:
            #we really need one of these for this calculation
            return
        if self.halfWidthSampleIndex is None:
            return
        
        primaryAmplitude = self.samples[self.primaryFeatureSampleIndex,1]
        threshold = 0.9 * abs( primaryAmplitude )
        
        #we jump right to the start of the halfwidth, and continue counting back
        #break when any of the following happens:
        # - index is 0 (start of samples)
        # - falls below 10% of peak amplitude
        # - waveform reverses direction
        # - the next sample is more than peaksContinuity away
        iStart = self.halfWidthSampleIndex[0]
        while   abs( primaryAmplitude - self.samples[ iStart, 1 ] )  < threshold:
            #edge check
            if iStart <= 0:
                break
            #direction change
            if abs(self.samples[ iStart-1,1]) > abs(self.samples[ iStart,1 ]):
                break
            #big jump check
            if self.samples[ iStart,0] - self.samples[ iStart-1,0] > self.peaksContinuity:
                break
            iStart -= 1
        
        self.riseTimeSampleIndex = iStart
        self.riseTime = self.samples[self.primaryFeatureSampleIndex,0] - self.samples[iStart,0]

    def calc_falltime( self ):
        if self.primaryFeatureSampleIndex is None:
            self.find_peaks()
        if self.primaryFeatureSampleIndex is None:
            #we really need one of these for this calculation
            return
        if self.halfWidthSampleIndex is None:
            return
        
        primaryAmplitude = self.samples[self.primaryFeatureSampleIndex,1]
        threshold = 0.9 * abs( primaryAmplitude )
        
        #we jump right to the end of the halfwidth, and continue counting forward
        #break when any of the following happens:
        # - index is 0 (start of samples)
        # - falls below 10% of peak amplitude
        # - waveform reverses direction
        # - the next sample is more than peaksContinuity away
        iStop = self.halfWidthSampleIndex[1]
        while   abs( primaryAmplitude - self.samples[ iStop, 1 ] )  < threshold:
            #edge check
            if iStop >= len( self.samples )-1:
                break
            #direction change
            if abs(self.samples[ iStop+1,1]) > abs(self.samples[ iStop,1 ]):
                break
            #big jump check
            if self.samples[ iStop+1,0] - self.samples[ iStop,0] > self.peaksContinuity:
                break
            iStop += 1
        
        self.fallTimeSampleIndex = iStop
        self.fallTime = self.samples[iStop,0] - self.samples[self.primaryFeatureSampleIndex,0]

    def calc_overshoot( self ):
        #can this calculation even be done?
        if self.peaks is None:
            #the peaks are a lie!
            return

        if len( self.peaks ) < 2:
            #not enough peaks to do anything
            return
        if self.primaryFeaturePeakIndex == len( self.peaks )-1 or self.primaryFeaturePeakIndex is None:
            #the primary feature is the last peak.  Nothing comes after it
            return

        #get information about the primary feature
        primeTime, primeAmplitude = self.peaks[ self.primaryFeaturePeakIndex ]
        if primeAmplitude > 0:
            primeSign = 1
        else:
            primeSign = -1

        #seach forward through the peaks for the first sign reversal
        iPeak = self.primaryFeaturePeakIndex
        while self.peaks[iPeak][1]*primeSign > 0:
            if iPeak >= len( self.peaks ) -1:
                break
            iPeak += 1
        
        #did we find one?
        if self.peaks[iPeak][1]*primeSign > 0 or iPeak == self.primaryFeaturePeakIndex: 
            #we did not
            return

        #keep moving forward so long as the overshoot is getting bigger
        while iPeak < len( self.peaks ) -1:

            #do not walk off the array
            if iPeak+1 >= len( self.peaks ):
                break
            
            #sign needs to stay oposite
            if self.peaks[iPeak+1][1]*primeSign > 0:
                break
            
            #peaks need to get bigger, not smaller
            if abs(self.peaks[iPeak+1][1]) > abs(self.peaks[iPeak][1]):
                iPeak += 1
            else:
                break

        #do the calculations
        self.overshootTime  = self.peaks[iPeak][0] - primeTime
        self.overshootRatio = abs( self.peaks[iPeak][1]/primeAmplitude )
        self.overshootPeakIndex = iPeak

    def calc_amplitude( self ):
        #we could probably eventually make this output in Amps, but that requires the calibration
        if self.peaks is None or self.primaryFeaturePeakIndex is None:
            return
        normalizedDistance = self.distance/100000
        self.normalizedAmplitude = self.peaks[ self.primaryFeaturePeakIndex, 1] * normalizedDistance

def byte2type( B ):
    '''
    This method reads in a byte, and converts to a type, for 1.9.4 compressed data

    Returns
    -------
    returns a int
    '''
    
    # is this a string or a number?  We can only shift numbers
    if  isinstance( B, str ):
        B, = struct.unpack( 'B', B )
    for i in range(8):
        if B >> 7-i == 1:
            return i
    # if we haven't already
    return i

def boundary_prev( level ):
    '''
    Method is used for the performing the floor function already defined.
    if we're already on the floor, we'll need to subtract 1 first

    Arguments
    ---------
    level: int
    '''

    i = 0
    output = level
    while output == level:
        output = boundary_floor( level-i )
        i += 1
    return output

def boundary_next( level ):
    # were we passed a valid level?
    level = boundary_floor( level )
    level += 2**( max(level.bit_length()-3,0) )
    #the digitizer is 14 bits, and the least 2 sig bits are meaningless
    while level %4 != 0:
        level += 2**( max(level.bit_length()-3,0) ) 
    return level

def boundary_floor( level ):
    '''
    what is the closest boundary lower than or equal to this level
    this is more complicated by a lot because the least 2 significant
    bits are meaningless in the digitizer (14 bit AD)

    Arguments
    ---------
    level: int
    '''

    level = level >> 2
    N = level.bit_length()
    if N <= 3:
        return level << 2
        # return level
    else:
        return (level >> N-3) << N-1
        # return (level >> N-3) << N-3
