#!/usr/bin/python
###
# Python 2/3 compatibility stuff
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from six import string_types
###############################################################################
#
# Copyright (c) 2017 Earth Networks, Inc.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

########################################################################
# FeedReceiver
# Lightning data feed receiver classes and functions
# Includes:
#   FeedReceiver    - Class for connecting to datafeed
#   Pulse           - Class for decoding Pulse information
#   Flash           - Class for decoding Flash information
#   checksum        - Computes checksum as used in Lx Data Feed
#   epoch2timestamp - converts Unix epoch time to UTC timestamp
#   timestamp2epochnano - converts UTC timestamp to Unix epoch time, and fractional nanoseconds
#
# All classes should have full Python 2 and Python 3 compatibility

# Imports
# numpy is used for building arrays, and for general math
# import numpy as np
# we really only need this to figure out where the location file is stored
import os, sys
# struct allows decoding binary data, needed for raw LTG files and binary feeds
import struct
# time and calendar are generic libraries for workign with time
import time, calendar, datetime
# json is a build in library for decoding json files
import json, numbers
# threading is only used by the FeedReciever class, and allows it to 
# operated asynchronously
import threading
import socket
# some of the files are compressed with gzip, this will decompress them
import gzip
#config parsing stuff
try:
    #python 3
    from configparser import ConfigParser
except:
    #python 2
    from ConfigParser import ConfigParser
import ast

#local import
from .PulseFlash import Flash, Pulse

DEBUGGING = False

class FeedReceiver( threading.Thread ):
    '''
    process that runs in th background creating stuctured data that provides updates of current information.
    A list of flashes, pulses or combos is created, either in binary or json.
    '''
    #dictionary to convert human readable options to the numbers the feed is expecting
    _feedConst = { 'flash':1, 'pulse':2, 'combo':3, 'ascii':1, 'binary':2 }

    def __init__( self, partnerId=None, **kwargs ): 
        '''
        initializes FeedReceiver attributes: self and any number of keyword arguments.
        arguments are set automatically if not specified. Default arguments are: 
            partnerId (mandatory)
            ip 
            port
            type	1: flash, 2: pulse, 3:combo (binary only)
            format	1: ascii, 2: binary
            showSource  Boolian
            meta        Boolian
            decode      Boolian
            config - if given indicates that a config file with initial parameters is given
        '''
        
        ###
        # initialize all the threading stuff
        threading.Thread.__init__(self)
        
        # best guess default settings
        self.partnerId = partnerId	#we really do need one
        self.ip        = ['107.23.135.182','107.23.152.248']    #defaults to testing feed, contact customer care for production end point
        self.port      = 2324
        self.type  = 'pulse'    #1: flash, 2: pulse, 3:combo (binary only)
        self.format= 'ascii'    #1: ascii, 2: binary
        self.version   = 3      #2: CSV, 3:JSON
        self.showSource= True   #shows type 40 for wwlln pulses
        self.meta      = True   #Adds additional info like error ellipse to feed output
        self.decode	   = True   #should we bother decoding the strings at all?
        self.connectionString = ""

        # initial parameters can also be set with a config file
        if 'config' in kwargs.keys():
            #then we have a config file:
            cfg = ConfigParser()
            cfg.read( kwargs['config'] )
            
            for section in cfg.sections():
                for key, value in cfg.items( section ):
                    value = ast.literal_eval( value )
                    setattr(self,key,value)

        # were any kwargs passed?
        for key in kwargs:
            setattr( self, key, kwargs[key] )	

        # special options, this normalizes the form of these
        self.type   = self.type.lower()
        self.format = self.format.lower()

        # which IP should we use?
        self.whichIp = 0
            
        ###
        # initialize the messages
        self.received = []
                
        ###
        # set daemon flag so ctrl-c will kill the program
        self.daemon = True

        ###
        # set a running flag, so we can turn the thread off
        self.isRunning = False

        ###
        # open the socket
        self.lastOpened = 0
        self.open_socket()

    def close_socket( self ):
        '''
        closes socket

        input 
            self
        '''
        ###
        # used with reopening socket
        self.socket.close()
    
    def open_socket( self ):
        '''
        opens socket to a server

        input 
            self
        '''

        #limit the rate we can open the socket
        while time.time()-self.lastOpened < 5:
            time.sleep( .1 )
        self.lastOpened = time.time()

        ###
        # the reciever has a socket set up to pull the data feed
        self.socket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
        self.socket.connect( (self.ip[self.whichIp], self.port) )

        ###
        # generate the request string that tells EN what to send us
        # note: python3 defaults to unicode strings, not what we want for the socket
        requestStr = b'{"p":"%s","v":%i,"f":%i,"t":%i,"class":3'%(self.partnerId.encode(), self.version, self._feedConst[self.format], self._feedConst[self.type] )
        #other options
        if self.meta:
            requestStr += b',"meta":true'
        if self.showSource:
            requestStr += b',"showSource":true'
        #close string
        requestStr += b'}'

        #a bit sloppy to have this labeled two different ways...
        self.connectionString = requestStr
        self.socket.sendall( requestStr )
        
        ###
        # clean up the sending part of the socket
        self.socket.shutdown(socket.SHUT_WR)

    def _loop_v2binary( self ):
        '''
        same as _loop_v3binary
        '''
        self._loop_v3binary()

    def _loop_v2ascii( self ):
        '''
        goes and gets a packet of data that is used as the message.
        appends message to flash or pulse string that is used for the Pulse and Flash classes. 
        
        handles both flash and pulse feeds.
        line format for both pulses and flashes is:
        <type>,<timestamp>,<latitude>,<longitude>,<peak current>,<reserved>,<height>,<number sensors>,<multiplicity>\r\n

        input 
            self
        output
            no direct output, instead creates updates to existing pulse or flash.
        '''

        #per documentation, each pulse (flash) is approximately 77 () bytes long.  
        #the fields are fixed width, and 0 padded.  But, in the unlikely event that 
        #a field exceeds the fixed width size allotted, the line will grow in length 
        #rather than collide.  This happens if the showSource:True option is used
        #each line ends with a carriage return and line feed, these can be used to 
        #split strings
        #line format for pulses and flashes is:
        #<type>,<timestamp>,<latitude>,<longitude>,<peak current>,<reserved>,<height>,<number sensors>,<multiplicity>\r\n
        #in the V2 feed, it is not possible to get extend information (flashes) or error ellipse information (pulses)
        message = b""
        while self.isRunning:

            # first thing we do, always is get a packet of data
            # get_packet handles what to do if the socket needs to be 
            # reset
            message = self._get_packet( message )
            
            # if the message is too short to get the length, we'll need 
            # another one
            if len(message) < 77:
                continue
            #do we have the line feed?
            if not b'\r\n' in message:
                continue

            ###
            # split out the message
            S = message.split( b'\r\n' )[0]
            #remove the parsed portion of the message
            message = message[ len(S)+2: ]
            
            #are we decoding the data?
            if not self.decode:
                #no we're not, just append the message to the list
                self.received.append( S )
                continue
            #apparently we are decoding, what type of message is this.
            #we can't differentiate based on the message string, so we have to 
            #trust the configuration
            if self.type == 'flash':
                self.received.append( Flash( S, version=2, format='ascii' ) )
            elif self.type == 'pulse':
                self.received.append( Pulse( S, version=2, format='ascii' ) )
            else:
                raise ValueError( 'FeedReceiver._loop_v2ascii: Unknown feed type: %s - unsure how to proceed'%self.type )
        
        #cleanup
        self.close_socket()

    def _loop_v3binary( self ):
        '''
        goes and gets a packet of data that is used as the message.
        appends messge to Pulse or Flash
        
        input 
            self
        output
            no direct output, instead creates updates to existing pulse or flash.
        '''

        message = b""
        while self.isRunning:

            # first thing we do, always is get a packet of data
            # get_packet handles what to do if the socket needs to be 
            # reset
            # Python2/3 note - Python 2 this is a string, Python 3 this is bytes
            message = self._get_packet( message )
            
            # if the message is too short to get the length, we'll need 
            # another one
            if len(message) < 2:
                continue
            
            # get the message length from the first 2 bytes of the message
            if self.type == 'combo':
                messageLen, = struct.unpack( '>H', message[:2] )
            else:
                # Python2/3 note - in Python3, the struct call can be replaced with message[0]
                # Python2/3 note - using struct on single elements requring indexing with slice
                messageLen, = struct.unpack( 'B', message[:1] )

            #make sure the message is long enough
            if len(message) < messageLen:
                continue

            ###
            # split out the message
            S = message[:messageLen]
            
            # decode the message?
            if not self.decode:
                #we don't decode at all
                self.received.append( S )
            elif self.type == 'pulse':
                self.received.append( Pulse( S, version=self.version, format='binary') )
            elif self.type == 'flash':
                self.received.append( Flash( S, version=self.version, format='binary') )
            elif self.type == 'combo':
                #this is handled in a flash object as well
                self.received.append( Flash( S, version=self.version, format='binary') )
            else:
                #i'm not sure what it is
                pass
            
            #update the message
            message = message[messageLen:]
        
        #clean up 
        self.close_socket()

    def _loop_v3ascii( self ):
        '''
        goes and gets a packet of data that is used as the message.
        appends message to flash or pulse string that is used for the Pulse and Flash classes. 
        
        input 
            self
        output
            no direct output, instead creates updates to existing pulse or flash.
        '''
        message = b""
        while self.isRunning:

            # first thing we do, always is get a packet of data
            # get_packet handles what to do if the socket needs to be 
            # reset
            message = self._get_packet( message )
            
            # if the message is too short to get the length, we'll need 
            # another one
            if len(message) < 4:
                continue
            
            # parse the message length so we know how much to get
            messageLen, = struct.unpack( '>I', message[:4] )

            #make sure the message is long enough
            if len(message) < messageLen:
                continue
            
            ###
            # split out the message, the first 4 bytes are the size
            S = message[4:messageLen]
            message = message[messageLen:]

            # now, what type of message is this.
            # we just use the config for this
            if not self.decode:
                #we don't decode at all
                self.received.append( S )
            elif self.type == 'flash':
                self.received.append( Flash(S, version=3, format='ascii') )
            elif self.type == 'pulse':
                self.received.append( Pulse(S, version=3, format='ascii') )
            else:
                raise ValueError( 'FeedReceiver._loop_v3ascii: Unknown feed type: %s - unsure how to proceed'%self.type )
        
        #cleanup
        self.close_socket()

    def _get_packet( self, message=b"" ):
        '''
        gets package of data. 
        if no data is received for over 20 secons an Exception is raised.
        else the data is turned into s message string that is then used in the methods 
        loop_binary, loop_binary_old, and loop_json.
        if the socket fails the feed is restarted.

        input
            self
            message			empty string that the data will be added to to create a message string
        output 
            message + S		"" + "appended data", big string that is used in classed Flash, Pulse and Combo
        '''
        S = b""
        tNow = time.time()

        #we try a number of times to actually get the next bit of data
        while len(S) == 0:
            S = self.socket.recv(4)
            if time.time() - tNow > 20:
                # it's been more than 20 seconds, and 
                # we haven't gotten a packet, not even a keepalive
                self.close_socket()
                self.whichIp = (self.whichIp+1)%len(self.ip)
                self.open_socket()
                return b""
        return message+S

            
    def run( self ):
        '''
        note: this method is not normally directly called.  Instead, this is initiated with 
        a ReedReceiver.start() call, where start() is inherited from threading.Thread

        input
            self
        '''
        if self.partnerId is None:
            raise Exception ( 'FeedReceiver.run : invalid Partner ID: %s'%repr(self.partnerId) )
        
        #set the running flag, we're officially running now!
        self.isRunning = True

        if self.format == 'ascii':
            # ascii feed
            if self.version == 2:
                self._loop_v2ascii()
            elif self.version == 3:
                self._loop_v3ascii()
            else:
                raise ValueError( "FeedReceiver.run: Unknown datafeed version: %s, not sure how to decode"%repr(self.version) )
        elif self.format == 'binary':
            # binary feed
            if self.version == 2:
                self._loop_v2binary()
            elif self.version == 3:
                self._loop_v3binary()
            else:
                raise ValueError( "FeedReceiver.run: Unknown datafeed version: %s, not sure how to decode"%repr(self.version) )
        else:
            raise ValueError("FeedReceiver.run: Unknown feed format:%i, not sure how to decode"%self.format)

    def stop( self ):
        self.isRunning = False
        self.close_socket()


########################
# Functions

def checksum( S ):
    """computes the checksum of ENTLN feed string
    """
    ###
    # computes the checksum as per EN documentation
    N = len(S)
    #this unpacks each byte
    l = struct.unpack( '%iB'%N, S )
    
    return (256-sum(l)%256)%256
