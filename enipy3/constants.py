#################################################################
# Fundamental Constants, 
# changing these requires changing physics
F            = 1.00    #this factor accounts for the speed of light being a little slower in air
C_MS         = 299792.458*F   #speed of light, m/ms
C_US         = 299.792458*F   #speed of light, m/us
C_NS         = 0.299792458*F  #speed of light, m/ns
R_EARTH      = 6371000 #meters (mean radius)
R_EARTH_POLE = 6356752  #for oblate spheroid calculations
R_EARTH_EQUA = 6378137
EARTH_FLATTENING = 0.0033528599338647
ARC_EARTH    = 111195  #meters/degree
Z0           = 377. #Impedance of free space


#related to the peak finder for ltg messages
DECIMATED_PEAKS_CONTINUITY      = 40000 #require this much deadtime before allowing a new peak
DECIMATED_PEAKS_AMPLITUDE_FACTOR = 1.50
DECIMATED_PEAKS_MAX_LENGTH       = 10
DECIMATED_PEAKS_NOISE_REJECTION_FACTOR = 1.50
# HF_PEAKS_CONTINUITY   = 40000
PEAKS_THRESHOLD_FACTOR= 0
