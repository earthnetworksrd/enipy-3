from distutils.core import setup
from distutils.extension import Extension
# from Cython.Distutils import build_ext
  
import numpy
 
setup( 
	name='enipy3', 
	version='3.0.2alpha',
	description='Python3 tools for Earth Networks Data',
	packages=['enipy3',],
	license=open('LICENSE.txt').read(),
	long_description=open('README.md').read()
	)
