###
# Python 2/3 compatibility stuff
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from email.mime import base
from io import IOBase, open   #needed for python 3 file instance checking
#needed for string instance checking
try: basestring
except NameError: 
    basestring = (str, bytes)
    
# from common import *
from enipy3.common import epoch2timestamp, timestamp2epochnano, string2number

#basic python libraries
import struct, datetime, json, time, ast, os
import numpy as np


class Report( ):
    """Report
    This is a memory efficient collection of pulses or flashes (not both)

    Note, this is largely a class wrapper around a np.ndarray.  It's not as fancy as, say,
    a dataframe, but it's not too bad.  
    """
    _altKeys = {     'time'         :'timeStamp',
                     'longitude'    :'lon',
                     'latitude'     :'lat',
                     'peakcurrent'  :'amplitude',
                     'numbersensors':'numsensors',
                     'icHeight'     :'altitude',
                     'ulLatitude'   :'minlatitude',
                     'ulLongitude'  :'minlongitude',
                     'lrLatitude'   :'maxlatitude',
                     'lrLongitude'  :'maxlongitude',
                     'minor'		:'eeminor',
                     'minoraxis'    :'eeminor',
                     'major'        :'eemajor',
                     'majoraxis'    :'eemajor',
                     'bearing'      :'eebearing',
                     'height'       :'altitude',
                     'peak_current' :'amplitude',
                     'ic_height'    :'altitude',
                     'ic_multiplicity':'icmultiplicity', 
                     'cg_multiplicigy':'cgmultiplicity',
                     'number_of_sensors':'numsensors'}
    
    def __init__( self, inputPath=None, grouping=None, format=None):
        if inputPath is None:
            self.inputPath = None
        elif isinstance( inputPath, basestring ):
            #this must be a path
            if os.path.exists( inputPath ):
                self.inputPath = inputPath
        else:
            raise Exception( 'Report.__init__: Error, input inputPath %s does not appear to be like a file'%repr(inputPath) )
        
        #initialization
        self._arr = None
        self.inputPath = inputPath
        self.format = format
        #I want the grouping string to be all lower case, for later truth tests.  
        if grouping is not None:
            self.grouping = grouping.lower()
        else:
            self.grouping = grouping

        #load data?
        if self.inputPath is not None:
            #state files
            if self.inputPath[-5:]=='state' or self.format=='state':
                self.load_state( self.inputPath )
            # TODO, add in none state file reading
            if self.format=='json':
                self.load_json( )
            if self.format=='oneminute':
                self.load_oneminute( )
            if self.format=='csv':
                self.load_csv( )

    def load_json( self ):
        ###
        # if the input file isn't given, we'll read the first one we 
        # have on hand.  
        if self.inputPath is None and len(self.inFiles) > 0:
            self.inputPath = self.inFiles[0]
        elif self.inputPath is None:
            raise( ValueError, 'No file given to read')
        
        ####
        i = 0
        with open(self.inputPath, 'r') as f:
            for line in f:
                jsonStr = line.strip().replace('\\','').replace('"{','{').replace('}"','}')
                if self.grouping == 'flash' or self.grouping==None: # if grouping is not specified, default to flashes
                    self.append( Flash(jsonStr,format='rawjson') )
                elif self.grouping == 'pulse':
                    # Extract the pulses from the flash
                    jsonDict = json.loads( jsonStr )
                    for pulse in jsonDict['pulses']:
                        try:
                            self.append(Pulse(str(pulse),format='rawjson'))
                        except ValueError:
                            # Probably loading in a python json string from lxlocate, which needs some extra re-formating to work
                            #print(str(pulse).replace('\'','"'))
                            self.append(Pulse(str(pulse).replace('\'','"'),format='rawjson'))
                elif self.grouping == 'solution':
                    # Extract the solutions from the flash
                    #jsonDict = json.loads( jsonStr )
                    #print(jsonStr)
                    self.append( Pulse(jsonStr,format='rawjson') )
    
    def load_oneminute(self ):
        ###
        # if the input file isn't given, we'll read the first one we 
        # have on hand.
        if self.inputPath is None and len(self.inFiles) > 0:
            self.inputPath = self.inFiles[0]
        elif self.inputPath is None:
            raise( ValueError, 'No file given to read')
        
        if 'Portions' in self.inputPath:
            # This is a pulse file
            self.grouping='pulse'
        else:
            # This is a flash file
            self.grouping='flash'
        
        i = 0
        with open(self.inputPath, 'r') as f:
            f.readline() # First line is the header, so move on
            if self.grouping == 'pulse':
                for line in f:
                    self.append(Pulse(line, format='oneminute') )
            if self.grouping == 'flash':
                for line in f:
                    self.append(Flash(line, format='oneminute') )
        self.update()
    
    def load_csv(self ):
        ###
        # if the input file isn't given, we'll read the first one we 
        # have on hand.
        if self.inputPath is None and len(self.inFiles) > 0:
            self.inputPath = self.inFiles[0]
        elif self.inputPath is None:
            raise( ValueError, 'No file given to read')
        
        if 'Pulse' in self.inputPath:
            # This is a pulse file
            self.grouping='pulse'
        else:
            # This is a flash file
            self.grouping='flash'
        
        i = 0
        with open(self.inputPath, 'r') as f:
            f.readline() # First line is the header, so move on
            if self.grouping == 'pulse':
                for line in f:
                    if line == '':
                        # Passed an empty file
                        break
                    self.append(Pulse(line, format='csv',version=2) )
                    i += 1
            if self.grouping == 'flash':
                for line in f:
                    if line == '':
                        # Passed an empty file
                        break
                    self.append(Flash(line, format='csv',version=1) )
                    i += 1
        if i > 0:
            # Only update if anything was added
            self.update()
    
    def load_state(self, inFile ):
        #set input path
        self.inputPath = inFile

        #load the array
        #this is done with a memmap, talk about low tech
        arr = np.memmap( inFile, dtype='double', mode='r' )
        
        #get the shape out of the header
        shape = arr[:2].astype(int)

        ###
        # get type information from the array
        # in state files, this is stupid simple
        if shape[1] == 15:
            #flash
            if self.grouping is not None and self.grouping != 'flash':
                #we got an issue
                raise Exception( 'Report.load_state: Error, state file grouping (flash) does not match report grouping (%s)'%self.grouping )
            self.grouping='flash'
        elif shape[1] == 10:
            if self.grouping is not None and self.grouping != 'pulse':
                #we got an issue
                raise Exception( 'Report.load_state: Error, state file grouping (pulse) does not match report grouping (%s)'%self.grouping )
            self.grouping='pulse'

        #copy this into memory
        self._arr = np.zeros( shape )
        self._arr[:] = arr[shape[1]:].reshape( shape )
        
        #update things, this will set all the correct values
        self.update()

    def save_state( self, outFile ):
        """
        save_state saves the output of the arrays directly to a file, 
        in a non-human readable form.  Reading these states is much 
        faster than reading the ascii format data file
        """	
        #the header here really only includes the shape of the array
        header = np.zeros( self._arr.shape[1] )
        header[:2] = self._arr.shape 
        shape = self._arr.shape[0]+1, self._arr.shape[1]
        #we then just dump the internal array to disk using a memmap, pretty crude
        arr = np.memmap( outFile, mode='write', shape=shape, dtype='double' )
        arr[0]  = header
        arr[1:] = self._arr
        arr.flush()

    def truncate( self, m ):
        #m is either a mask, or an int
        if isinstance( m, int ):
            #it's a number, cut off the first m entries in the array
            self._arr = self._arr[m:]
        elif isinstance( m, (list, np.ndarray) ):
            #it's a mask, probably
            try:
                self._arr = self._arr[m]
            except:
                raise ValueError('Report.truncate: Error, invalid mask')
        else:
            raise ValueError('Report.truncate: Error, Truncate takes an int or a mask')
        
        #update the helper arrays
        self.update()

    def append( self, ob, update=True ):
        #ob might be a list of obs
        if isinstance( ob, list ):
            for subOb in ob:
                self.append( subOb, update=False )
            # update the helper arrays
            if update:
                self.update()
            return
        #ob might be another report
        elif isinstance( ob, Report ):
            
            if self._arr is None:
                #we don't have any data yet.  Probably we should just copy the data over
                if self.grouping is None or self.grouping == ob.grouping:
                    #we don't have any data, just copy data from the other report over
                    #self.strType = ob.strType
                    self.grouping = ob.grouping
                    self._arr = ob._arr.copy()
                else:
                    raise Exception( 'Attempting to append Report of grouping %s to Report to grouping %s'%(ob.grouping, self.grouping) )
            elif self.grouping is not None and self.grouping == ob.grouping:
                #we do have data
                N = ob._arr.shape
                s = self._arr.shape
                
                #double check that they're compatible
                if s[1] != N[1]:
                    #not sure how this can happen, but it just did
                    raise Exception ('%s is incompatible'%repr( ob ) )
                
                # the helper arrays are referencing the memory in the 
                # array.  We'll update them in a second, so for now
                # we'll ignore the refcheck at our own peril				
                self._arr.resize( [s[0]+N[0], s[1]], refcheck=False )
                #hopefully I counted the length correctly
                self._arr[s[0]:] = ob._arr
            else:
                raise Exception ('%s is incompatible'%repr( ob ) )
    
        elif self.grouping == 'pulse' and isinstance( ob, Flash ):
            if len( ob.pulses ) > 0:
                self.append( ob.pulses, update=False )

        elif self.grouping == 'pulse' and isinstance( ob, Pulse ):
            arr = self.decode( ob )
            if self._arr is not None:
                N = self._arr.shape
                self._arr.resize( [N[0]+1, N[1]], refcheck=False )
                self._arr[N[0]:] = arr
            else:
                N = arr.shape[0]
                self._arr = arr.reshape( [1,N] ).copy()

        elif self.grouping == 'flash' and isinstance( ob, Flash ):
            arr = self.decode( ob )
            if self._arr is not None:
                N = self._arr.shape
                self._arr.resize( [N[0]+1, N[1]], refcheck=False )
                self._arr[N[0]:] = arr
            else:
                N = arr.shape[0]
                self._arr = arr.reshape( [1,N] ).copy()

        elif self.grouping == None:
            #then we haven't created the array yet
            if isinstance( ob, Pulse):
                self.grouping = 'pulse'
            elif isinstance( ob, Flash ):
                self.grouping = 'flash'

            arr = self.decode( ob )
            N = arr.shape[0]
            self._arr = arr.reshape( [1,N] ).copy()
        
        else:
            raise ValueError('%s is incompatible'%repr( ob ))
        
        # update the helper arrays
        if update:
            self.update()

    def decode( self, ob ):

        if self.grouping == 'pulse':
            arr = np.zeros( 10 )
        elif self.grouping == 'flash':
            arr = np.zeros( 15 )
        else:
            raise ValueError('Report.decode: unknown grouping %s'%repr(self.grouping) )

        #shared fields
        arr[0] = ob.type
        arr[1] = ob.time
        arr[2] = ob.lat
        arr[3] = ob.lon
        arr[4] = ob.amplitude
        arr[5] = ob.altitude
        arr[6] = ob.numsensors

        #flash specific fields
        if self.grouping == 'flash':
            arr[7]  = ob.icmultiplicity
            arr[8]  = ob.cgmultiplicity
            arr[9]  = ob.starttime
            arr[10] = ob.endtime
            arr[11] = ob.minlatitude
            arr[12] = ob.maxlatitude
            arr[13] = ob.minlongitude
            arr[14] = ob.maxlongitude
        #pulse specific field
        elif self.grouping == 'pulse':
            arr[7] = ob.eemajor
            arr[8] = ob.eeminor
            arr[9] = ob.eebearing

        return arr

    def update(self):
        #this sets the helper strings
        if self.grouping == None:
            #then no data could have been set yet, just return
            return None
        
        #clear my attributes
        for k in self._altKeys:
            if k in dir(self):
                delattr( self, k )
        
        self.type      = self._arr[:,0]
        self.time      = self._arr[:,1]
        self.lat       = self._arr[:,2]
        self.lon       = self._arr[:,3]
        self.amplitude = self._arr[:,4]
        self.height    = self._arr[:,5]
        self.numsensors= self._arr[:,6]
        
        # now things get strType specific
        if self.grouping == 'flash':
            self.icmultiplicity = self._arr[:,7]
            self.cgmultiplicity = self._arr[:,8]
            self.starttime      = self._arr[:,9]
            self.endtime        = self._arr[:,10]
            self.minlatitude     = self._arr[:,11]
            self.minlongitude    = self._arr[:,12]
            self.maxlatitude     = self._arr[:,13]
            self.maxlongitude    = self._arr[:,14]
        elif self.grouping == 'pulse':
            self.eemajor        = self._arr[:,7]
            self.eeminor        = self._arr[:,8]
            self.eebearing	    = self._arr[:,9]
        else:
            raise Exception( 'Report.update: Error, unkown report grouping %s'%self.grouping)

        # alternate namings
        for key in self._altKeys:
            if hasattr( self, key ):
                #set the standardized version
                setattr( self, self._altKeys[key], getattr( self, key) )
    
    def pop_event( self, m ):
        '''
        Returns a flash/pulse indicated by an index
        '''
        
        #m is either a mask, or an int
        if isinstance( m, int ):
            timeStamp = epoch2timestamp( self.time[m] ) + '.%09i'%(1000000000*(self.time[m]%1))
            if self.grouping == 'pulse':
                pulse = Pulse()
                pulse.version       = None # Does not exists in Reports
                pulse.format        = 'report'

                #initialize all possible values for a pulse, in case the message string doesn't include all information
                pulse.type      = self.type[m]         #0 CG, 1 IC, 40 WWLLN
                pulse.latitude  = self.lat[m]          #in decimal degrees
                pulse.longitude = self.lon[m]          #in decimal degrees
                pulse.timestamp = timeStamp            #does not exists in reports, but I made it
                pulse.time      = self.time[m]         #unix epoch time - float number of seconds since 1970. Only precise to microseconds
                pulse.amplitude = self.amplitude[m]    #in Amps
                pulse.altitude  = self.altitude[m]     #in meters above sealevel
                pulse.numsensors= self.numsensors[m]   #the number of sensors which contributed to the solution
                pulse.eemajor   = self.eemajor[m]      #error ellipse major axis, in km
                pulse.eeminor   = self.eeminor[m]      #error ellipse minor axis, in km 
                pulse.eebearing = self.eebearing[m]    #error ellipse bearing in degrees
                try:
                    pulse.epoch     = self.epoch[m]        #unix epoch time - integer number of seconds since 1970
                    pulse.nano      = self.nano[m]         #integer number of nanoseconds after the epoch 
                except AttributeError:
                    # This is an ENIPY3 sttribute. Probably loading data from ENIPY2
                    pulse.epoch     = int(pulse.time)
                    pulse.nano      = int(1000000000*(pulse.time%1))
                pulse._update_special_attributes(v2=True)
                return pulse
            elif self.grouping == 'flash':
                flash = Flash()
                flash.version       = None # Does not exists in Reports
                flash.format        = 'report'

                
                #initialize all possible values for a flash, in case the message string doesn't include all information
                flash.type      = self.type[m]              #0 CG, 1 IC, 40 WWLLN
                flash.latitude  = self.lat[m]               #in decimal degrees
                flash.longitude = self.lon[m]               #in decimal degrees
                flash.timestamp = timeStamp                 #does not exists in reports, but I made it
                flash.amplitude = self.amplitude[m]       #in Amps
                flash.height    = self.altitude[m]          #in meters above sealevel
                flash.altitude  = self.altitude[m]          #in meters above sealevel
                flash.numsensors= self.numsensors[m]        #the number of sensors which contributed to the solution
                flash.icmultiplicity=self.icmultiplicity[m] #number of IC pulses included in this flash
                flash.cgmultiplicity=self.cgmultiplicity[m] #number of CG pulses included in this flash
                flash.maxlatitude= self.maxlatitude[m]      #upper left (max) latitude
                flash.minlatitude= self.minlatitude[m]      #lower right (min) latitude
                flash.minlongitude=self.minlongitude[m]     #upper left (min) longitude
                flash.maxlongitude=self.maxlongitude[m]     #lower right (max) longitude
                flash.time      = self.time[m]
                flash.starttime = self.starttime[m]         #unix epoch time - float number of seconds since 1970. Only precise to microseconds
                flash.endtime   = self.endtime[m]           #unix epoch time - float number of seconds since 1970. Only precise to microseconds
                # flash.duration  = self.duration[m]          #duration of the flash in nanoseconds, apparently isn't in reports
                try:
                    flash.epoch     = self.epoch[m]             #unix epoch time - integer number of seconds since 1970
                    flash.nano      = self.nano[m]              #integer number of nanoseconds after the epoch 
                    flash.startepoch= self.startepoch[m]        #unix epoch time - int number of seconds since 1970.
                    flash.startnano = self.startnano[m]         #integer number of nanoseconds after the startepoch 
                    flash.endepoch  = self.endepoch[m]          #unix epoch time - int number of seconds since 1970.
                    flash.endnano   = self.endnano[m]           #integer number of nanoseconds after the startepoch 
                except AttributeError:
                    # This is an ENIPY3 sttribute. Probably loading data from ENIPY2
                    flash.epoch     = int(flash.time)
                    flash.nano      = int(1000000000*(flash.time%1))
                    flash.startepoch= int(flash.starttime)
                    flash.startnano = int(1000000000*(flash.starttime%1))
                    flash.endepoch  = int(flash.endtime)
                    flash.endnano   = int(1000000000*(flash.endtime%1))
                flash._update_special_attributes()#v2=True)
                return flash
        else:
            raise ValueError('Report.pop_event: Error, Pop_event takes an int')
        
        #update the helper arrays
        self.update()

    def write_csv( self, filelike, mode='w', header=True ):
        """writes the output to a file, using the semi-standard csv format 
        used by EN R&D for a long time now
        """

        #open the input file for reading
        if isinstance( filelike, basestring ):
            #it's a string, try and open
            outFile = open( filelike, mode )

        elif hasattr( filelike, 'write' ):
            #then it's a file
            outFile = filelike
        else:
            raise Exception( '%s does not seem to be writeable'%repr(filelike) )

        if self.grouping == 'pulse':
            #do pulse stuff
            self._write_csv_pulse( outFile, header )
        elif self.grouping == 'flash':
            #do flash stuff
            self._write_csv_flash( outFile, header )
        else:
            raise Exception( 'Unknown grouping %s for csv output'%(repr(self.grouping)) )

    def _write_csv_pulse( self, outFile, header=True ):

        f = outFile
        
        # first we write the header line
        if header:
            f.write('type,timestamp,latitude,longitude,peakcurrent,icheight,numbersensors,majoraxis,minoraxis,bearing\n')
        
        # then we loop through the sources, and write each line
        for i in range( len( self.time ) ):
            S = ''
            S += ('%i, '%self.type[i]).rjust( 4 )
            S += ('%s, '%epoch2timestamp( self.time[i] ) + '.%09i'%((self.time[i]%1)*1000000000)).rjust(30)
            S += ('%3.6f, '%self.lat[i]).rjust( 12 )
            S += ('%3.6f, '%self.lon[i]).rjust( 12 )
            S += ('%i, '%self.amplitude[i]).rjust( 8 )
            S += ('%i, '%self.height[i]).rjust( 8 )
            S += ('%i, '%self.numsensors[i]).rjust( 5 )
            S += ('%i, '%self.eemajor[i]).rjust( 5 )
            S += ('%i, '%self.eeminor[i]).rjust( 5 )
            S += ('%i  '%self.eebearing[i]).rjust( 5 )
            S += '\n'
            #~ print S
            f.write(S)
        
    def _write_csv_flash( self, outFile, header=True  ):
        f = outFile
        
        if header:
            # first we write the header line
            f.write( 'type,timestamp,latitude,longitude,peakcurrent,icheight,numbersensors,icmultiplicity,cgmultiplicity,starttime,endtime,duration,ullatitude,ullongitude,lrlatitude,lrlongitude\n' )
        """
        type,timestamp,latitude,longitude,peakcurrent,
        icheight,numbersensors,icmultiplicity,cgmultiplicity,
        starttime,endtime,duration,ullatitude,ullongitude,lrlatitude,lrlongitude\n
        """
        for i in range( len( self.time ) ):
            S = ''
            S += ('%i, '%self.type[i]).rjust( 4 )
            S += ('%s, '%epoch2timestamp( self.time[i] ) + '.%09i'%((self.time[i]%1)*1000000000)).rjust(30)
            S += ('%3.6f, '%self.lat[i]).rjust( 12 )
            S += ('%3.6f, '%self.lon[i]).rjust( 12 )
            S += ('%i, '%self.amplitude[i]).rjust( 8 )
            S += ('%i, '%self.height[i]).rjust( 6 )
            S += ('%i, '%self.numsensors[i]).rjust( 5 )
            S += ('%i, '%self.icmultiplicity[i]).rjust( 5 )
            S += ('%i, '%self.cgmultiplicity[i]).rjust( 5 )
            S += ('%s, '%epoch2timestamp( self.starttime[i] ) + '.%09i'%((self.starttime[i]%1)*1000000000)).rjust(30)
            S += ('%s, '%epoch2timestamp( self.endtime[i] ) + '.%09i'%((self.endtime[i]%1)*1000000000)).rjust(30)
            S += ('%1.6f, '%(self.endtime[i]-self.starttime[i])).rjust( 10 )
            S += ('%3.6f, '%self.ullatitude[i]).rjust( 12 )
            S += ('%3.6f, '%self.ullongitude[i]).rjust( 12 )
            S += ('%3.6f, '%self.lrlatitude[i]).rjust( 12 )
            S += ('%3.6f  '%self.lrlongitude[i]).rjust( 12 )
            S += '\n'
            #~ print S
            f.write(S)


class Pulse( ):
    """Pulse
    This is a general class for an ENTLN located 'pulse'.  One lightning 
    'flash' is made up of many 'pulses'.  
    """

    #alt keys provides a mapping of various alternative forms of values into a standardized form
    #the standard form is always then available in the pulse object
    _altKeys = {     'lat'          :'latitude',
                     'latitude'     :'lat',
                     'lon'          :'longitude',
                     'longitude'    :'lon',
                     'icHeight'     :'altitude',
                     'ic_height'    :'altitude',
                     'height'       :'altitude',
                     'amplitude  '  :'peakcurrent',
                     'numbersensors':'numsensors',
                     'number_of_sensors':'numsensors',
                     'ulLatitude'   :'minlatitude',
                     'ulLongitude'  :'minlongitude',
                     'lrLatitude'   :'maxlatitude',
                     'lrLongitude'  :'maxlongitude',
                     'minor'        :'eeminor',
                     'minoraxis'    :'eeminor',
                     'major'        :'eemajor',
                     'majoraxis'    :'eemajor',
                     'bearing'      :'eebearing',
                     'ic_multiplicity':'icmultiplicity', 
                     'cg_multiplicigy':'cgmultiplicity' }

    def __init__( self, messageString=None, version=3, format='ascii', header=None ):
        '''
        initializes pulse instance
        input:
            messageString   the string to decode
            version         2 or 3, the version of lxdatafeed
            format          'ascii', 'binary', 'csv', 'json', 'flat'
            '''

        # store the initializing string, just in case
        if isinstance( messageString, bytes ): messageString = messageString.decode()   #python3 compatibility shit for gzip files
        self.messageString = messageString
        self.version       = version
        self.format        = format
        self.grouping      = 'pulse'

        #initialize all possible values for a pulse, in case the message string doesn't include all information
        self.type      = None   #0 CG, 1 IC, 40 WWLLN
        self.latitude  = None   #in decimal degrees
        self.longitude = None   #in decimal degrees
        self.timestamp = None   #string YYYY-MM-DDTHH:MM:SS.nnnnnnnnn   (except in v2 ascii)
        self.time      = None   #unix epoch time - float number of seconds since 1970. Only precise to microseconds
        self.epoch     = None   #unix epoch time - integer number of seconds since 1970
        self.nano      = None   #integer number of nanoseconds after the epoch 
        self.amplitude = None   #in Amps
        self.altitude  = None   #in meters above sealevel
        self.numsensors= None   #the number of sensors which contributed to the solution
        self.eemajor   = 0      #error ellipse major axis, in km
        self.eeminor   = 0      #error ellipse minor axis, in km 
        self.eebearing = 0      #error ellipse bearing in degrees
        self.header    = header 
        if self.header is not None: self.header = self.header.strip()
        self.strokesolution=None

        #do we have a message string to decode?
        if self.messageString is None:
            return

        if self.format == 'ascii':
            if self.version == 2:
                self._decode_v2ascii()
            elif self.version == 3:
                self._decode_v3ascii()
            else:
                raise ValueError('Pulse.__init__: unknown version: %i for format %s - can not decode'%(self.version, repr(self.format)))
        elif self.format == 'binary':
            if self.version == 2:
                self._decode_v2binary()
            elif self.version == 3:
                self._decode_v3binary()
            else:
                raise ValueError('Pulse.__init__: unknown version: %i for format %s - can not decode'%(self.version, repr(self.format)))
        elif self.format == 'csv':
            if header is not None:
                self._decode_csv( )
            #otherwise we have some built in headers based on the version
            elif self.version == 1:
                self.header = 'type,timestamp,latitude,longitude,peakcurrent,icheight,numbersensors,majoraxis,minoraxis,bearing'
                self._decode_csv( )
            elif self.version == 2:
                self.header = 'FlashPortionID,FlashPortionGUID,FlashGUID,Lightning_Time,Lightning_Time_String,Latitude,Longitude,Height,Stroke_Type,Amplitude,Stroke_Solution,Offsets,Confidence,LastModifiedTime,LastModifiedBy'
                self._decode_csv( )
            else:
                raise ValueError('Pulse.__init__: unknown version: %i for format %s - can not decode'%(self.version, repr(self.format)))
        elif self.format == 'oneminute':
            self._decode_oneminute( )
        elif self.format == 'flat':
            self._decode_flat( )
        elif self.format == 'edw1':
            self._decode_edw1( )
        elif self.format == 'rawjson':
            self._decode_v4rawjson( )
            # else:
            #     raise ValueError('Pulse.__init__: unknown version: %i for format %s - can not decode'%(self.version, repr(self.format)))
        else:
            ###
            # I don't know what format this is
            raise ValueError('Pulse.__init__: unknown format: %s - can not decode'%repr(self.format))

    def _decode_csv( self ):
        if self.header is None:
            #we don't know what field goes where, how'd this happen?
            raise Exception ('Pulse._decode_csv: header fields not set for csv decoding' )
        
        header = self.header.split( ',' )
        messageFields = self.messageString.split(',' )
        if len(header) != len(messageFields):
            raise Exception ('Pulse._decode_csv: header and message do not match')
        
        for i in range( len(header) ):
            #convert to a number, using a simple try except block
            try:
                #ast is a way of safely evaluating a literal.
                #this will act to guess the type of the string value
                value = ast.literal_eval( messageFields[i].strip() )
            except:
                #if ast raised an error, it must be a string.  this is usually a timestamp
                value = messageFields[i].strip()

            setattr( self, header[i].strip().lower(), value )
        
        self._update_special_attributes()

    def _decode_oneminute( self ):
        """
        This data format is legacy and not very good.  But it's used by 
        the one-minute data-ingest routine, and the R&D archive.  
        """
        ###
        #Fields
        #    FlashPortionID,           Sequential number used by DB
        #    FlashPortionGUID,         GUID for pulse
        #    FlashGUID,                GUID for parent flash
        #    Lightning_Time,           time object in DB, now just time in different format
        #    Lightning_Time_String,    time stamp, accurate to 1ns
        #    Latitude,                 float, degrees
        #    Longitude,                float, degrees
        #    Height,                   int, meters 
        #    Stroke_Type,              0 = CG, 1 = IC.  Type 40 not explicit
        #    Amplitude,                int, amperes
        #    Stroke_Solution,          json (see below)
        #    Offsets,                  string
        #    Confidence,               int, 0-100, meaningless
        #    LastModifiedTime,         timestamp
        #    LastModifiedBy            "LtgInsertFlashPortion_pr"
        #In general, this is a csv file.  But, the stroke solution and offsets both have ',' in them
        #also, sometimes the stroke solution is truncated
        #Stroke_Solution
        #    "{"ee":{"maj":<float>,"min":<float>,"b":<float>},
        #    "v":"1.0.0.0",
        #    "ns":<int>,
        #    "so":{"station_id":<int>,...}}"
        #Offsets
        #    "station_id=<int>,...,"
        fields = self.messageString.split(',')
        self.pulseid   = fields[0]
        self.pulseguid = fields[1]
        self.flashguid = fields[2]
        self.lightningtime = fields[3]
        self.timestamp = fields[4]
        self.latitude  = float( fields[5] )
        self.longitude = float( fields[6] )
        self.altitude  = float( fields[7] )
        self.type      = int( fields[8] )
        self.amplitude = float( fields[9] )
        #stroke solution, how far do we search?
        #we key off the beginning and end of the json string
        iStrokeSolution = 10
        while fields[iStrokeSolution][-2:] != '}"':
            iStrokeSolution += 1
        self.strokesolution = ','.join( fields[10:iStrokeSolution+1] )
        self._decode_strokesolution()
        #offsets - here we can count from the end
        self.offsets = ','.join( fields[iStrokeSolution+1:-3] )
        self.confidence = fields[-3]
        self.lastmodifiedtime = fields[-2]
        self.lastmodifiedby   = fields[-1]

        self._update_special_attributes( )

    def _decode_flat( self, flatTimeStamp=None ):
        """
        The flat format is a legacy format used to archive lightning location data going all the way back to 2010
        It's just a csv dump of the fields in the database holding the location information, but the format of the 
        data in the columns of the database change over time.  

        This is a really annoying format to deal with
        """
        ###
        # Flat files are exactly the same as one-minute files, except that the stroke-solution keeps being repurposed over time

        fields = self.messageString.split(',')
        #check the timestamp
        try:
            timestamp2epochnano( fields[4] )
        except:
            #in 2009, there are some archives that have an extra field at the beginning
            fields = fields[1:]

        self.pulseid   = fields[0].strip()
        self.pulseguid = fields[1].strip()
        self.flashguid = fields[2].strip()
        self.lightningtime = fields[3].strip()
        self.timestamp = fields[4].strip()
        self.latitude  = float( fields[5] )
        self.longitude = float( fields[6] )
        self.altitude  = float( fields[7] )
        self.type      = int( fields[8] )
        self.amplitude = float( fields[9] )

        self.confidence = fields[-3]
        self.lastmodifiedtime = fields[-2]
        self.lastmodifiedby   = fields[-1]

        #get decoding timestamp
        if flatTimeStamp is None:
            flatTimeStamp = self.timestamp

        #now we get to deal with the stroke solution, fun

        if flatTimeStamp > '2013-09-02T00' and flatTimeStamp < '2013-09-03T00':
            # print ('2013/09/02 special case')
            #there's a single day format relating to this, which adds in error ellipse info for a little bit
            self.strokesolution = ','.join( fields[10:13] )
            self.offsets = ','.join( fields[13:-3] )
            self._decode_2013_strokesolution()
        elif flatTimeStamp < '2014-06-05T19:46:28.052045613':
            # print ('Early Days')
            #Early Days
            #  Old format, example
            #  Sensor: offset@err(num;dev;+peak;-peak)=JCKSN:3264@1.175(42;0;4698;-4770) DMPLS:3578@2.295(26;0;547;-661) COVNT:3613@-0.467(15;0;313;-399) RUSTE:3723@0.699(28;0;1291;-1273) HRNND:3744@3.051(17;0;226;-270) CLLMN:4053@0.541(13;0;129;-135) JCKGP:4694@-0.801(7;0;;-105)
            #  Note, no escape quotation marks
            #  The first part of the string defines the format, string can be truncated
            #Pulse Example:
            #672634100,5c369319-7248-41c1-bf8d-d3ce3bedd96f,8cb6eb82-e84f-48a0-ac28-902292130a4d,12/12/2010 12:00:00 AM,2010-12-12T00:00:00.518843024,32.5250016,-90.0249713,8083.3,1,-587,Sensor: offset@err(num;dev;+peak;-peak)=JCKSN:3264@1.175(42;0;4698;-4770) DMPLS:3578@2.295(26;0;547;-661) COVNT:3613@-0.467(15;0;313;-399) RUSTE:3723@0.699(28;0;1291;-1273) HRNND:3744@3.051(17;0;226;-270) CLLMN:4053@0.541(13;0;129;-135) JCKGP:4694@-0.801(7;0;;-105) ,"JCKSN=3264,DMPLS=3578,COVNT=3613,RUSTE=3723,HRNND=3744,CLLMN=4053,JCKGP=4694,",100,12/11/2010 7:00:04 PM,LtgInsertFlashPortion_pr
            self.strokesolution = fields[10]
            self.offsets = ','.join( fields[11:-3] )
            self._decode_2010_strokesolution()
        else:
            # print ('Modern Days?')
            #Intermediate Days
            #  Stroke solution was replaced with a json object containing version and error ellipse information
            #  But, the 'LocationError' portion was still appended to the end
            #Pulse Example:
            #1586058645,537a1899-c523-47fc-bb72-4fb72431b662,75d80769-b5a2-4036-84d9-ec49ce89d973,6/5/2014 7:46:28 PM,2014-06-05T19:46:28.556804716,36.6063325,-72.1594877,17080.5,1,6111,"{"ee":{"maj":0.227,"min":0.1,"b":71.6},"v":"3.0.0.6","ns":0,"so":null};LocationError=0.356;","EXMRE=0,CPMYM=66,MRGRC=83,MRLSP=126,MHEWS=138,KLMRN=180,VINBE=208,DNDRN=286,MNMTB=338,DRXHL=403,MNMTH=422,NRTHS=423,",100,6/5/2014 3:46:57 PM,LtgInsertFlashPortion_pr
            #Modern Days
            #  Same as above, but without the 'LocationError' field
            #stroke solution, how far do we search?
            #we key off the beginning and end of the json string
            iStrokeSolution = 10
            while iStrokeSolution < len( fields )- 3 :
                if fields[iStrokeSolution][-2:] == '}"':
                    #this is the end of the stroke solution of the modern type
                    break
                if 'LocationError' in fields[iStrokeSolution]:
                    #intermediate days end of stroke solution field
                    break
                iStrokeSolution += 1

            self.strokesolution = ','.join( fields[10:iStrokeSolution+1] )
            self._decode_strokesolution()
            #offsets - here we can count from the end
            self.offsets = ','.join( fields[iStrokeSolution+1:-3] )

        self._update_special_attributes( )


    def _decode_2010_strokesolution( self ):
        #we can detect wwlln solutions from the timestamp with these solutions
        #in the beginning, the timestamps for wwlln solutions were shorter
        #during this time, there was a period in 2010 when all stroke solutions were empty for some reason
        if self.timestamp < '2013-06-05T00:00:00' and len( self.timestamp ) < 29 and self.strokesolution.strip() == "":
            self.type = 40
        #that changed on 2013/06/05, after this the stroke solution is always populated for non-wwlln strokes
        elif self.timestamp < '2013-06-05T00:00:00' and self.strokesolution.strip() == "":
            self.type = 40
        if 'LocationError=' in self.strokesolution:
            # print (self.timestamp, 'Early+ Days' )
            locationError = float( self.strokesolution.split( 'LocationError=' )[1][:-1] )
            self.eemajor = locationError
            self.eeminor = locationError
        else:
            pass
            # print (self.timestamp, 'Early Days' )
    
    def _decode_2013_strokesolution( self ):
        #this format shows up for 1 day of data only, because of course it does
        #strokesolution = "{"errorEllipse":{"majorAxis":2.923,"minorAxis":0.249,"bearing":79.8}}"
        
        # print (self.timestamp, 'Special Day' )
        headerConversion = {
            "errorEllipse":"errorellipse",
        }
        dic = json.loads( self.strokesolution.strip('"') )
        for key in dic:
            if key in headerConversion:
                setattr( self, headerConversion[key], dic[key] )
        if self.errorellipse != None:
            self.eeminor = self.errorellipse['majorAxis']
            self.eemajor = self.errorellipse['minorAxis']
            self.eebearing = self.errorellipse['bearing']
        else:
            #there doesn't seem to be any wwlln data on this day, 
            #so this statement is not needed, but just in case.
            self.type=40

    def _decode_strokesolution( self ):
        ###
        # the stroke solution is a json string used by the engineers to shove data they don't have a field in the DB to put
        # it's stupid
        # Intermediate days, there's also a 'LocationError' field, not in json format for reasons
        if ';LocationError=' in self.strokesolution:
            locationError = float( self.strokesolution.split( ';LocationError=' )[1].strip( '"' ).strip( ';' ) )
            strokeSoltuion = self.strokesolution.split( ';LocationError=' )[0]
            # print (self.timestamp, 'Intermediate Days' )
        else:
            strokeSoltuion = self.strokesolution
            locationError = None
            # print (self.timestamp, 'Modern Days' )
        try:
            dic = json.loads( strokeSoltuion.strip('"') )
            headerConversion = {
                "ee":"errorellipse",
                "v":"version",
                "ns":"numsensors",
                "so":"strokeoffsets"
            }
            for key in dic:
                if key in headerConversion:
                    setattr( self, headerConversion[key], dic[key] )
            
            #WWLLN solutions have no error ellipse information
            if self.errorellipse != None:
                self.eeminor = self.errorellipse['min']
                self.eemajor = self.errorellipse['maj']
                self.eebearing = self.errorellipse['b']
            else:
                self.type=40
        except:
            #fall back on locationError
            if locationError is not None:
                self.eemajor = locationError
                self.eeminor = locationError


    def _decode_v2ascii( self ):
        '''
        line format for both pulses and flashes is:
        <type>,<timestamp>,<latitude>,<longitude>,<peak current>,<reserved>,<height>,<number sensors>,<multiplicity>\r\n

        input 
            self
        output
            no direct output, populates attributes
        '''
        fields = self.messageString.decode().split( ',' )
        self.type      = int(   fields[0])
        self.timestamp = fields[1].strip()
        self.latitude  = float( fields[2])
        self.longitude = float( fields[3])
        self.amplitude =float( fields[4])
        #fields[5] is reserved
        self.height    = float( fields[6])
        self.numsensors= int(   fields[7])
        #fields[8] is multiplicity, but this is always 0 for pulses

    def _decode_v2binary( self ):
        '''
        decodes a binary pulse (string input is binary)
        '''
        #we're working with a binary string, not human readable
    
        # A pulse message should be 26 bytes long, including the following:
        # !! all numbers are encoded big endian !!		
        # 0 	length		unsigned int	56 (for flash)
        # 1		type		unsigned int	0 CG, 1 IC, 9 keep alive
        # 2-5	time (s)	unsigned int	epoc time of the strongest pulse (CG if available)
        # 6-7	time (ms)	unsigned int	ms of the second
        # 8-11	lat			signed int		lat *10,000,000, positive N, negative S
        # 12-15	lon			signed int		lon *10,000,000, positive E, negative W
        # 16-19 current		signed int		in Amperes
        # 20                Reserved
        # 21-22 height		unsigned int	in meters
        # 23	sensors		unsigned int
        # 24                multiplicity    (not used in pulses)
        # 25	check sum	unsigned int	Check sum
        self.length,        = struct.unpack(  'B', self.messageString[0:1] )
        #check that the length is what we expect
        if self.length != 26:
            #it seems that this message string is malformed
            raise Exception( 'Pulse._decode_v2binary: message string malformed - invalid size: %i'%self.length )

        self.type,          = struct.unpack(  'B', self.messageString[1:2] )
        self.epoch,         = struct.unpack( '>I', self.messageString[2:6] )
        self.nano           = struct.unpack( '>H', self.messageString[6:8] )[0]*1000000

        self.latitude       = struct.unpack( '>i', self.messageString[8:12] )[0]/10000000.
        self.longitude      = struct.unpack( '>i', self.messageString[12:16] )[0]/10000000.
        self.amplitude,     = struct.unpack( '>i', self.messageString[16:20] )
        self.height,        = struct.unpack( '>H', self.messageString[21:23] )
        self.numsensors,    = struct.unpack(  'B', self.messageString[23:24] )
        #self.messageString[21:23] - not used in pulses
        self.checksum,      = struct.unpack(  'B', self.messageString[25:] )

        #check that the checksum matches
        if checksum( self.messageString[:-1] ) != self.checksum:
            raise ValueError('Pulse._decode_v2binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ) )

        #special fields (time)
        self._update_special_attributes( )

    def _decode_v3ascii( self ):
        '''
        decode json pulse (string input in json format)
        '''

        #then we're working with a json string
        #pulse feed
        #{ "time":"2017-02-26T17:32:29.817443878Z",
        #  "type":0,
        #  "latitude":-21.5043742,
        #  "longitude":-49.1724032,
        #  "peakCurrent":-21356.0,
        #  "icHeight":0.0,
        #  "numSensors":16,
        #  "eeMajor":278.0,
        #  "eeMinor":200.0,
        #  "eeBearing":21.8
        #}
        
        # first we decode the json string, this puts everything into a dictionary
        jsonDict = json.loads( self.messageString )

        #apply all the keys as attributes to self
        #this works so easy because the attributes of this object are named the same 
        #as the attributes of the json string (almost), the outliers we handle with 
        #_update_sepcial_attributes
        for key in jsonDict.keys():
            setattr( self, key.lower(), jsonDict[key] )
        
        #special fields (time)
        self._update_special_attributes( )

    def _decode_v3binary( self ):
        '''
        decodes a binary pulse (string input is binary)
        '''
        #we're working with a binary string, not human readable
    
        # A flash message should be 32 bytes long, including the following:
        # !! all numbers are encoded big endian !!		
        # 0 	length		unsigned int	32 (for flash)
        # 1		type		unsigned int	0 CG, 1 IC, 9 keep alive
        # 2-5	time (s)	unsigned int	epoc time of the strongest pulse (CG if available)
        # 6-9	time (ns)	unsigned int	ns of the second
        # 10-13	lat			signed int		lat *10,000,000, positive N, negative S
        # 14-17	lon			signed int		lon *10,000,000, positive E, negative W
        # 18-21 current		signed int		in Amperes
        # 22-23 height		unsigned int	in meters
        # 24	sensors		unsigned int
        # 25-26	err major	unsigned int	Error elipse major axis, meters
        # 27-28	err minot	unsigned int	Error elipse minor axis, meters
        # 29-30	err bearing	unsigned int	Error elipse bearing, degrees
        # 31	check sum	unsigned int	Check sum
        self.length,        = struct.unpack(  'B', self.messageString[0:1] )
        if self.length != 32:
            #it seems that this message string is malformed
            raise Exception( 'Pulse._decode_v3binary: message string malformed - invalid size: %i'%self.length )
        
        self.type,          = struct.unpack(  'B', self.messageString[1:2] )
        self.epoch,         = struct.unpack( '>I', self.messageString[2:6] )
        self.nano,          = struct.unpack( '>I', self.messageString[6:10] )
        self.latitude       = struct.unpack( '>i', self.messageString[10:14] )[0]/10000000.
        self.longitude      = struct.unpack( '>i', self.messageString[14:18] )[0]/10000000.
        self.amplitude,     = struct.unpack( '>i', self.messageString[18:22] )
        self.height,        = struct.unpack( '>H', self.messageString[22:24] )
        self.numsensors,    = struct.unpack(  'B', self.messageString[24:25] )
        self.eemajor,       = struct.unpack( '>H', self.messageString[25:27] )
        self.eeminor,       = struct.unpack( '>H', self.messageString[27:29] )
        self.eebearing,     = struct.unpack( '>H', self.messageString[29:31] )
        self.checksum,      = struct.unpack(  'B', self.messageString[31:] )

        #check that the checksum matches
        if checksum( self.messageString[:-1] ) != self.checksum:
            raise ValueError( 'Pulse._decode_v3binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ))

        #special fields (time)
        self._update_special_attributes( )
        
    def _decode_edw1( self ):
        """
        This data format is used by engineering.
        #In general, this is a csv file.  The stroke solution and offsets do not exist in this format.
        """
        ###
        #Fields
        #    PulseID,                  Sequential number used by DB
        #    Timestamp,                time stamp, accurate to 1ns
        #    Latitude,                 float, degrees
        #    Longitude,                float, degrees
        #    Altitude,                 int, meters
        #    Type,                     0 = CG, 1 = IC.  Type 40 not explicit
        #    Peak Current,             int, amperes
        #    Confidence,               int, 0-100, meaningless
        #    Number of sensors,        int, number of sensors that located the largest pulse
        #    Error Ellipse Major Axis, float, km
        #    Error Ellipse Minor Axis, float, km
        #    Error Ellipse Bearing,    float, degrees clockwise from North
        
        
        fields = self.messageString.split(',')
        self.pulseID   = fields[0]
        self.timestamp = fields[2]
        self.latitude  = float( fields[3] )
        self.longitude = float( fields[4] )
        self.altitude  = float( fields[5] )
        self.type      = int( fields[6] )
        self.amplitude = float( fields[7] )
        self.confidence= int(fields[8])
        try:
            self.numsensors = int(fields[9])
            self.eemajor = float(fields[10])
            self.eeminor = float(fields[11])
            self.eebearing = float(fields[12])
        except ValueError:
            self.numsensors = 0
            self.eemajor = 0
            self.eeminor = 0
            self.eebearing = 0

        self._update_special_attributes( )

    def _decode_v4rawjson( self ):
        """
        raw json format is what the V2.0 stack outputs to, and what is ingest into the v4 datafeed
        The json output of this stack is a combo flash, which contains a list of pulses.  
        This method decodes the pulse object only
        Note: this is mostly an internal format
        """
        #this is what a flash object (with 1 pulse) looks like
        """{
            "flashGuid":"e0205e5d-7aa3-4509-a93f-cf0441b29d3d",
            "timeStamp":"2021-06-10T23:59:46.736578249",
            "type":1,
            "version":"2.0.7",
            "latitude":14.968615,
            "longitude":83.556749,
            "height":9339,
            "amplitude":-4246.0,
            "cgMultiplicity":0,
            "icMultiplicity":1,
            "wwllnMultiplicity":0,
            "confidence":2.46,
            "numSensors":7,
            "startTimeStamp":"2021-06-10T23:59:46.736578249",
            "endTimeStamp":"2021-06-10T23:59:46.736578249",
            "duration":0.0,
            "boundingBox":[[14.968615,14.968615],[83.556749,83.556749]],
            "pulses":[
                {
                    "height":9339,
                    "id":"36a164c8-4e39-419c-af7f-7924e7bcb152",
                    "timeStamp":"2021-06-10T23:59:46.736578249",
                    "type":1,
                    "version":"2.0.7",
                    "latitude":14.968615,
                    "longitude":83.556749,
                    "altitude":9339,
                    "amplitude":-4246.0,
                    "fifthNearestSensorDistance":620345,
                    "solutionCount":2,
                    "chisq":2.066,
                    "confidence":2.46,
                    "numSensors":7,
                    "residual":[1102,-397,-644,256,-364,1795,-1747],
                    "stationAmplitudes":{"N3KLM":-300,"N4HLK":-154,"N5RCH":-248,"N3TRC":-216,"N2NGL":-506,"N5KLB":-186,"MDRLK":-255},
                    "stationOffsets":{"N3KLM":739445885,"N4HLK":741357504,"N5RCH":738857723,"N3TRC":739089767,"N2NGL":737862212,"N5KLB":739152154,"MDRLK":739291365},
                    "stationsOn":9,
                    "stationsDetecting":["N2NGL","N5RCH","N3TRC","N5KLB","MDRLK","N3KLM","N4HLK"],
                    "errorEllipse":{"majorAxis":450.0,"minorAxis":212.0,"majorAxisBearing":140.0},
                    "altTimeCorrection":7.4,
                    "altQuality":1,
                    "altAlgorithm":1,
                    "biggestAngle":153,
                    "waveformSignature":null,
                    "classificationAlg":""
                    }
                ],
            "buddies":null,
            "buddiesTimeStamps":null
            }"""
        #because this is a json object, we can use the same base methods as used in V3
        # first we decode the json string, this puts everything into a dictionary
        try:
            jsonDict = json.loads( self.messageString )
        except json.decoder.JSONDecodeError:
            # Apparently json cannot handle None values, so need to replace those with null
            jsonDict = json.loads( self.messageString.replace('None','null') )
        
        try:
            jsonDict.keys()
        except AttributeError:
            # json string is probably coming from python output, which is slightly wrong
            # do some reformatting for it to work.
            jsonDict = json.loads( self.messageString.strip().replace('\\','').replace('"{','{').replace('}"','}') )

        #apply all the keys as attributes to self
        #this works so easy because the attributes of this object are named the same 
        #as the attributes of the json string (almost), the outliers we handle with 
        #_update_sepcial_attributes
        for key in jsonDict.keys():
            
            value = jsonDict[key]
            key = key.lower()

            if isinstance( value, basestring ):
                #lets see if we can convert this to a number
                try:
                    value = string2number( value )
                except:
                    #I guess we keep it a string
                    pass
            setattr( self, key, value )
        
        #the format of the error ellipse information is a little different
        try:
            if self.errorellipse is not None:
                self.eemajor = self.errorellipse['majorAxis']
                self.eeminor = self.errorellipse['minorAxis']
                self.eebearing = self.errorellipse['majorAxisBearing']
        except AttributeError: # Handle new field naming convention as of v2.1.2(?)
            if self.ee is not None:
                self.eemajor = self.ee['eea']
                self.eeminor = self.ee['eei']
                self.eebearing = self.ee['eeb']
        
        if self.timestamp == None: # Handle new field naming convention as of v2.1.2(?)
            self.timestamp = self.ts
            self.numsensors = self.nsens
        
        #special fields (time)
        self._update_special_attributes( )

    def _update_special_attributes( self , v2=False):
        '''
        Updates a small number of special fields which don't come directly 
        from the feed message string
        '''
        for key in self._altKeys:
            if hasattr( self, key ):
                #set the standardized version
                setattr( self, self._altKeys[key], getattr( self, key) )
        # #json peculiarities:
        # #height, is names 'icheight'
        # if hasattr( self, 'icheight' ):
        #     self.height = self.icheight
        # #timestamp is name time
        # if hasattr( self, 'time' ):
        #     self.timestamp = self.time
        if type(self.lat) == str:
            self.lat = float(self.lat)
            self.lon = float(self.lon)

        # If v2 is true, it means that we are loading data from ENIPY2, which does not have epoch and nano, so we skip this part.
        if not v2:
            #convert timestamp to epoch and nano, or vice versa (depending on binary or ascii)
            if self.epoch is None:
                #try generating from time stamp,
                #this happens in ascii feeds
                self.epoch, self.nano = timestamp2epochnano( self.timestamp )
                self.time = self.epoch + self.nano/1.e9
            elif self.timestamp is None:
                #generate the timestamp from the epoch and nano
                self.timestamp = epoch2timestamp( self.epoch ) + '.%09i'%self.nano
    
    def _serialize_oneminute( self ):
        '''
        Outputs a string with formating matching the 1-min files
        '''        
        if not hasattr(self,'strokesolution'):
            strokeSolution = self._serialize_stroke_solution()
        elif self.strokesolution == None:
            strokeSolution = self._serialize_stroke_solution()
        else:
            strokeSolution = self.strokesolution
        
        row = [self.pulseid,self.pulseguid,self.flashguid,self.lightningtime,self.timestamp,str(self.latitude),str(self.longitude),str(self.altitude),str(self.type),str(self.amplitude),strokeSolution,self.offsets,str(self.confidence),self.lastmodifiedtime,self.lastmodifiedby]
        
        return ','.join(row)
    
    def _serialize_stroke_solution( self ):
        '''
        Outputs a string matching the typical stroke solution format
        '''
        #Check if this pulse as a valid error ellipse to use, if not try and generate one.
        if not hasattr(self,'eeminor'):
            errorEllipseString = 'null'
        elif self.eeminor == None:
            errorEllipseString = 'null'
        else:
            errorEllipseString = '{"maj":'+str(self.eemajor)+',"min":'+str(self.eeminor)+',"b";'+str(self.eebearing)+'}'
        
        #Check if this pulse as a valid stroke version to use.
        if not hasattr(self, 'version'):
            version = 'null'
        elif self.version == None:
            version = 'null'
        else:
            version = self.version
        
        #Check if this pulse as valid offsets to use, if not try and generate one.
        if not hasattr(self,'offsets'):
            offsetsString = None
        elif self.offsets == None:
            offsetsString = 'null'
        else:
            offsetsString = str(self.strokeoffsets).replace("'",'"').replace(' ','')
        
        strokeSolutionStr = '"{"ee":'+errorEllipseString+',"v":"'+version+'","ns":'+str(self.numsensors)+',"so":'+str(offsetsString)+'}"'
        
        return strokeSolutionStr
        
    def printout(self):
        '''
        returns string of formatted decoded data
        '''

        S = ""
        S += ('%2i,'    %self.type       ).rjust(4)
        S += (' %s,'    %self.timestamp  ).rjust(22)
        S += (' %5.4f,' %self.latitude   ).rjust(12)
        S += (' %5.4f,' %self.longitude  ).rjust(12)
        S += (' %9i,'   %self.peakcurrent).rjust(11)
        S += (' %5i,'   %self.height     ).rjust(6)
        S += (' %i'     %self.numsensors ).rjust(3)

        ###
        # error ellipse info may not be available
        if self.eemajor is None: return S


        S += ','
        S += (' %3.1f,' %self.eemajor   ).rjust(8)
        S += (' %3.1f,' %self.eeminor   ).rjust(8)
        S += (' %3.1f ' %self.eebearing ).rjust(8)
        return S

    # def serialize( self, version=self.version, format=self.format ):
    #     #the easy case, and also a common case
    #     if version==self.version and format==self.format:
    #         return self.messageString

    #     #otherwise, we're doing a format conversion
    #     raise NotImplementedError( 'format conversion during serialization not implemented yet' )

class Flash( ):
    '''
    Flash
    One lightning 'flash' is made up of many 'pulses'.
    '''

    _altKeys = {     'longitude'    :'lon',
                     'latitude'     :'lat',
                     'amplitude'    :'peakcurrent',
                     'numbersensors':'numsensors',
                     'icHeight'     :'altitude',
                     'ulLatitude'   :'minlatitude',
                     'ulLongitude'  :'minlongitude',
                     'lrLatitude'   :'maxlatitude',
                     'lrLongitude'  :'maxlongitude',
                     'minor'		:'eeminor',
                     'minoraxis'    :'eeminor',
                     'major'        :'eemajor',
                     'majoraxis'    :'eemajor',
                     'bearing'      :'eebearing',
                     'height'       :'altitude',
                     'height'       :'altitude',
                     'ic_height'    :'altitude',
                     'ic_multiplicity':'icmultiplicity', 
                     'cg_multiplicigy':'cgmultiplicity',
                     'number_of_sensors':'numsensors'}

    def __init__( self, messageString=None, version=3, format='ascii' ):
        '''
        initializes pulse instance
        input:
            messageString   the string to decode
            version         2 or 3, the version of lxdatafeed
            format          'ascii' or 'binary'
            '''

        # store the initializing string, just in case
        if isinstance( messageString, bytes ): messageString = messageString.decode()   #python3 compatibility shit for gzip files
        self.messageString = messageString
        self.version       = version
        self.format        = format
        self.grouping      = 'flash'

        #initialize all possible values for a flash, in case the message string doesn't include all information
        self.type      = None   #0 CG, 1 IC, 40 WWLLN
        self.latitude  = None   #in decimal degrees
        self.longitude = None   #in decimal degrees
        self.timestamp = None   #string YYY-MM-DDTHH:MM:SS.nnnnnnnnn (except v2)
        self.epoch     = None   #unix epoch time - integer number of seconds since 1970
        self.nano      = None   #integer number of nanoseconds after the epoch 
        self.amplitude =None   #in Amps
        self.height    = None   #in meters above sealevel
        self.numsensors= None   #the number of sensors which contributed to the solution
        self.multiplicity=None  #icMultiplicity + cgMultiplicity
        self.icmultiplicity=None #number of IC pulses included in this flash
        self.cgmultiplicity=None #number of CG pulses included in this flash
        self.ullatitude= None   #upper left (max) latitude
        self.lrlatitude= None   #lower right (min) latitude
        self.ullongitude=None   #upper left (min) longitude
        self.lrlongitude=None   #lower right (max) longitude
        #there's a lot more time stamps for a flash, but they're not always set
        self.starttimestamp=None #Time of the first pulse in the flash
        self.starttime = None   #unix epoch time - float number of seconds since 1970. Only precise to microseconds
        self.endtime   = None   #unix epoch time - float number of seconds since 1970. Only precise to microseconds
        self.endtimestamp= None #time of the last pulse in the flash
        self.startepoch= None   #unix epoch time - int number of seconds since 1970.
        self.startnano = None   #integer number of nanoseconds after the startepoch 
        self.duration  = 0      #duration of the flash in nanoseconds
        self.pulses    = []     #a list of pulse objects (may be empty)


        #do we have a message string to decode?
        if self.messageString is None:
            return

        if self.format == 'ascii':
            if self.version == 2:
                self._decode_v2ascii()
            elif self.version == 3:
                self._decode_v3ascii()
            else:
                raise ValueError( 'Flash.__init__: Unknown pulse version %s - can not decode'%repr(self.version))
        elif self.format == 'binary':
            if self.version == 2:
                self._decode_v2binary()
            elif self.version == 3:
                self._decode_v3binary()
            else:
                raise ValueError( 'Flash.__init__: Unknown pulse version %s - can not decode'%repr(self.version))
        elif self.format == 'edw1':
            self._decode_edw1( )
        elif self.format == 'rawjson':
            self._decode_v4rawjson( )
        #for flashes, oneminute and flat are the same
        elif self.format == 'oneminute' or self.format=='flat':
            self._decode_oneminute( )
        elif self.format == 'flat':
            self._decode_flat( )
        else:
            ###
            # I don't know what format this is
            raise ValueError('Flash.__init__: unknown format: %s - can not decode'%repr(self.format))

    def _decode_v2ascii( self ):
        '''
        line format for both pulses and flashes is:
        <type>,<timestamp>,<latitude>,<longitude>,<peak current>,<reserved>,<height>,<number sensors>,<multiplicity>\r\n

        input 
            self
        output
            no direct output, populates attributes
        '''
        fields = self.messageString.decode().split( ',' )
        self.type      = int(   fields[0])
        self.timestamp = fields[1].strip()
        self.latitude  = float( fields[2])
        self.longitude = float( fields[3])
        self.amplitude =float( fields[4])
        #fields[5] is reserved
        self.height    = float( fields[6])
        self.numsensors= int(   fields[7])
        self.multiplicity=int(  fields[8] )

        self._update_special_attributes( )

    def _decode_v2binary_flash( self, inputString ):
        '''
        decodes a binary pulse (string input is binary)
        '''

        # A flash message should be 26 bytes long, including the following:
        # !! all numbers are encoded big endian !!		
        # 0 	length		unsigned int	56 (for flash)
        # 1		type		unsigned int	0 CG, 1 IC, 9 keep alive
        # 2-5	time (s)	unsigned int	epoc time of the strongest pulse (CG if available)
        # 6-7	time (ms)	unsigned int	ms of the second
        # 8-11	lat			signed int		lat *10,000,000, positive N, negative S
        # 12-15	lon			signed int		lon *10,000,000, positive E, negative W
        # 16-19 current		signed int		in Amperes
        # 20                Reserved
        # 21-22 height		unsigned int	in meters
        # 23	sensors		unsigned int
        # 24                multiplicity    (not used in pulses)
        # 25	check sum	unsigned int	Check sum
        self.length,        = struct.unpack(  'B', inputString[0:1] )
        #check that the length is what we expect
        if self.length != 26:
            #it seems that this message string is malformed
            raise Exception( 'Pulse._decode_v2binary: message string malformed - invalid size: %i'%self.length )

        self.type,          = struct.unpack(  'B', inputString[1:2] )
        self.epoch,         = struct.unpack( '>I', inputString[2:6] )
        self.nano           = struct.unpack( '>H', inputString[6:8] )[0]*1000000

        self.latitude       = struct.unpack( '>i', inputString[8:12] )[0]/10000000.
        self.longitude      = struct.unpack( '>i', inputString[12:16] )[0]/10000000.
        self.amplitude,     = struct.unpack( '>i', inputString[16:20] )
        self.height,        = struct.unpack( '>H', inputString[21:23] )
        self.numsensors,    = struct.unpack(  'B', inputString[23:24] )
        self.multiplicity,  = struct.unpack(  'B', inputString[24:25] )
        self.checksum,      = struct.unpack(  'B', inputString[25:] )

        #check that the checksum matches
        if checksum( inputString[:-1] ) != self.checksum:
            raise ValueError('Flash._decode_v2binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ) )

        #special fields (time)
        self._update_special_attributes( )

    def _decode_v2binary( self ):
        '''
        decodes a binary pulse (string input is binary)
        works for binary flash feeds and binary combo feeds
        '''
        #we're working with a binary string, not human readable
        #this could be a flash, or is could be a combo thing
        #for flashes messages, the first byte will define the size, and that size will be 56
        #for combination messages, the first 2 bytes will define the size
        # 0-1    length      unsigned int    greater than 56+32 (for combo messages)
        # 2-57   flash       flash message
        # 58-89  pulse_1     pulse message
        # 90-121 pulse_2     pulse message
        # ...
        # last byte         check sum

        #is this message a flash?
        length, 		= struct.unpack(  'B', self.messageString[0:1] )
        if length == 26:
            #this message is a flash
            self._decode_v2binary_flash( self.messageString )
            #there are no pulses, return now
            return
        
        #if it's not a flash, then lets see if it's a combo message
        length, 		= struct.unpack(  '>H', self.messageString[0:2] )
        #the length should be 2 + 26 + N*26 + 1, if not something is missing
        if (length-29)%26 !=0:
            #it seems that this message string is malformed
            raise Exception( 'Flash._decode_v2binary: message string malformed - invalid size: %i'%length )
        
        #get the checksum out of the way too
        #check that the checksum matches
        self.checksum,      = struct.unpack(  'B', self.messageString[-1:] )
        if checksum( self.messageString[:-1] ) != self.checksum:
            raise ValueError('Flash._decode_v2binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ))

        #the flash portion of the flash is always in the same place:
        self._decode_v2binary_flash( self.messageString[2:28] )

        #now decode all the pulse sections
        iPulse = 54
        while iPulse < length-1:
            pulse = Pulse( messageString=self.messageString[iPulse:iPulse+26], version=2, format='binary' )
            self.add_pulse( pulse )
            iPulse += 26

    def _decode_v3ascii( self):
        '''
        decode json pulse (string input in json format)
        '''

        #then we're working with a json string
        #flash feed example:

        # first we decode the json string, this puts everything into a dictionary
        jsonDict = json.loads( self.messageString )

        #apply all the keys as attributes to self
        #this works so easy because the attributes of this object are named the same 
        #as the attributes of the json string (almost), the outliers we handle with 
        #_update_sepcial_attributes
        for key in jsonDict.keys():
            setattr( self, key.lower(), jsonDict[key] )
        
        #special fields (time)
        self._update_special_attributes( )

    def _decode_v3binary_flash( self, inputString ):
        '''
        decodes the flash portion of binary flash and combo strings
        '''

        ###
        # A flash message should be 56 bytes long, including the following:
        # !! all numbers are encoded big endian !!
        # 0 	length		unsigned int	56 (for flash)
        # 1		type		unsigned int	0 CG, 1 IC, 9 keep alive
        # 2-5	time (s)	unsigned int	epoc time of the strongest pulse (CG if available)
        # 6-9	time (ns)	unsigned int	ns of the second
        # 10-13	lat			signed int		lat *10,000,000, positive N, negative S
        # 14-17	lon			signed int		lon *10,000,000, positive E, negative W
        # 18-21 current		signed int		in Amperes
        # 22-23 height		unsigned int	in meters
        # 24	sensors		unsigned int
        # 25	IC mult		unsigned int	Number of IC pulses
        # 26	CG mult		unsigned int	Number of CG pulses
        # 27-30	start (s)	unsigned int	Start time (epoc)
        # 31-34 start (ns)	unsigned int	Start time fractional part
        # 35-38 Duration 	unsigned int	Duration of flash in ns
        # 39-42	UL lat		unsigned int	Upper Left corner latitude
        # 43-46	UL lon		unsigned int	Upper Left corner longitude
        # 47-50	UL lon		unsigned int	Lower Right corner latitude
        # 51-54	UL lon		unsigned int	Lower Right corner longitude
        # 55	Check sum	unsigned int	check sum
        

        self.length, 		= struct.unpack(  'B', inputString[0:1] )
        #check that the length is what we expect
        if self.length != 56:
            #it seems that this message string is malformed
            raise Exception( 'Flash._decode_v3binary_flash: message string malformed - invalid size: %i'%self.length )

        self.type,          = struct.unpack(  'B', inputString[1:2] )
        self.epoch,         = struct.unpack( '>I', inputString[2:6] )
        self.nano,          = struct.unpack( '>I', inputString[6:10] )
        self.latitude       = struct.unpack( '>i', inputString[10:14] )[0]/10000000.
        self.longitude      = struct.unpack( '>i', inputString[14:18] )[0]/10000000.
        self.amplitude,     = struct.unpack( '>i', inputString[18:22] )
        self.height,        = struct.unpack( '>H', inputString[22:24] )
        self.numsensors,    = struct.unpack( 'B', inputString[24:25] )
        self.icmultiplicity,= struct.unpack( 'B', inputString[25:26] )
        self.cgmultiplicity,= struct.unpack( 'B', inputString[26:27] )
        
        self.startepoch,    = struct.unpack( '>I', inputString[27:31] )
        self.startnano,     = struct.unpack( '>I', inputString[31:35] )
        self.duration,      = struct.unpack( '>I', inputString[35:39] )

        self.ullatitude     = struct.unpack( '>i', inputString[39:43] )[0]/10000000.
        self.ullongitude    = struct.unpack( '>i', inputString[43:47] )[0]/10000000.
        self.lrlatitude     = struct.unpack( '>i', inputString[47:51] )[0]/10000000.
        self.lrlongitude    = struct.unpack( '>i', inputString[51:55] )[0]/10000000.
        
        self.checksum,      = struct.unpack(  'B', inputString[55:] )

        #check that the checksum matches
        if checksum( inputString[:-1] ) != self.checksum:
            raise ValueError('Flash._decode_v3binary_flash: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ))

        #special fields (time)
        self._update_special_attributes( )

    def _decode_v3binary( self ):
        '''
        decodes a binary pulse (string input is binary)
        works for binary flash feeds and binary combo feeds
        '''
        #we're working with a binary string, not human readable
        #this could be a flash, or is could be a combo thing
        #for flashes messages, the first byte will define the size, and that size will be 56
        #for combination messages, the first 2 bytes will define the size
        # 0-1    length      unsigned int    greater than 56+32 (for combo messages)
        # 2-57   flash       flash message
        # 58-89  pulse_1     pulse message
        # 90-121 pulse_2     pulse message
        # ...
        # last byte         check sum

        #is this message a flash?
        length, 		= struct.unpack(  'B', self.messageString[0:1] )
        if length == 56:
            #this message is a flash
            self._decode_v3binary_flash( self.messageString )
            #there are no pulses, return now
            return
        
        #if it's not a flash, then lets see if it's a combo message
        length, 		= struct.unpack(  '>H', self.messageString[0:2] )
        #the length should be 2 + 56 + N*32 + 1, if not something is missing
        if (length-59)%32 !=0:
            #it seems that this message string is malformed
            raise Exception( 'Flash._decode_v3binary: message string malformed - invalid size: %i'%length )
        
        #get the checksum out of the way too
        #check that the checksum matches
        self.checksum,      = struct.unpack(  'B', self.messageString[-1:] )
        if checksum( self.messageString[:-1] ) != self.checksum:
            raise ValueError('Flash._decode_v3binary: Bad Checksum: %i %i'%(checksum( self.messageString[:-1] ), self.checksum ))

        #the flash portion of the flash is always in the same place:
        self._decode_v3binary_flash( self.messageString[2:58] )

        #now decode all the pulse sections
        iPulse = 58
        while iPulse < length-1:
            pulse = Pulse( messageString=self.messageString[iPulse:iPulse+32], version=3, format='binary' )
            self.add_pulse( pulse )
            iPulse += 32
    
    def _decode_edw1( self ):
        """
        This data format is used by engineering.
        #In general, this is a csv file.  The stroke solution and offsets do not exist in this format. Also, not clear of the duration (always 0) or LastModifiedBy (always empty) are actually set.
        """
        ###
        #Fields
        #    FlashID,                  Sequential number used by DB
        #    timestamp,                time stamp, accurate to 1ns
        #    Latitude,                 float, degrees
        #    Longitude,                float, degrees
        #    Altitude,                 int, meters
        #    Type,                     0 = CG, 1 = IC.  Type 40 not explicit
        #    Amplitude,                int, amperes
        #    Confidence,               int, 0-100, meaningless
        #    Start Time,               time stamp of the first pulse, accurate to 1ns
        #    End Time,                 time stamp of the last pulse, accurate to 1ns
        #    Number of sensors,        int, number of sensors that located the largest pulse
        #    IC Multiplicity,          int, number of IC pulses in the flash
        #    CG Multiplicity,          int, number of CG pulses in the flash
        #    Maximum Latitude,         float, degrees
        #    Minimum Latitude,         float, degrees
        #    Maximum Longitude,        float, degrees
        #    Minimum Longitude,        float, degrees
        #    Duration,                 int, seems to always be 0.
        #    LastModfiedBy,            string, seems to always be empty
        
        
        fields = self.messageString.split(',')
        self.flashID   = fields[0]
        self.timestamp = fields[2]
        self.latitude  = float( fields[3] )
        self.longitude = float( fields[4] )
        self.altitude  = float( fields[5] )
        self.type      = int( fields[6] )
        self.amplitude = float( fields[7] )
        self.confidence = int(fields[8])
        self.starttimestamp = fields[9]
        self.endtimestamp = fields[10]
        self.numsensors = int(fields[11])
        self.icmultiplicity = int(fields[12])
        self.cgmultiplicity = int(fields[13])
        self.maximumlatitude = float(fields[14])
        self.minimumlatitude = float(fields[15])
        self.maximumlongitude = float(fields[16])
        self.minimumlongitude = float(fields[17])
        self.duration = float(fields[18])
        self.LastModifiedBy = fields[19].strip()

        self._update_special_attributes( )

    def _decode_v4rawjson( self ):
        """
        raw json format is what the V2.0 stack outputs to, and what is ingest into the v4 datafeed
        The json output of this stack is a combo flash, which contains a list of pulses.  
        This method decodes both the flash and the pulses inside it
        Note: this is mostly an internal format
        """
        #this is what a flash object (with 1 pulse) looks like
        """{
            "flashGuid":"e0205e5d-7aa3-4509-a93f-cf0441b29d3d",
            "timeStamp":"2021-06-10T23:59:46.736578249",
            "type":1,
            "version":"2.0.7",
            "latitude":14.968615,
            "longitude":83.556749,
            "height":9339,
            "amplitude":-4246.0,
            "cgMultiplicity":0,
            "icMultiplicity":1,
            "wwllnMultiplicity":0,
            "confidence":2.46,
            "numSensors":7,
            "startTimeStamp":"2021-06-10T23:59:46.736578249",
            "endTimeStamp":"2021-06-10T23:59:46.736578249",
            "duration":0.0,
            "boundingBox":[[14.968615,14.968615],[83.556749,83.556749]],
            "pulses":[
                {
                    "height":9339,
                    "id":"36a164c8-4e39-419c-af7f-7924e7bcb152",
                    "timeStamp":"2021-06-10T23:59:46.736578249",
                    "type":1,
                    "version":"2.0.7",
                    "latitude":14.968615,
                    "longitude":83.556749,
                    "altitude":9339,
                    "amplitude":-4246.0,
                    "fifthNearestSensorDistance":620345,
                    "solutionCount":2,
                    "chisq":2.066,
                    "confidence":2.46,
                    "numSensors":7,
                    "residual":[1102,-397,-644,256,-364,1795,-1747],
                    "stationAmplitudes":{"N3KLM":-300,"N4HLK":-154,"N5RCH":-248,"N3TRC":-216,"N2NGL":-506,"N5KLB":-186,"MDRLK":-255},
                    "stationOffsets":{"N3KLM":739445885,"N4HLK":741357504,"N5RCH":738857723,"N3TRC":739089767,"N2NGL":737862212,"N5KLB":739152154,"MDRLK":739291365},
                    "stationsOn":9,
                    "stationsDetecting":["N2NGL","N5RCH","N3TRC","N5KLB","MDRLK","N3KLM","N4HLK"],
                    "errorEllipse":{"majorAxis":450.0,"minorAxis":212.0,"majorAxisBearing":140.0},
                    "altTimeCorrection":7.4,
                    "altQuality":1,
                    "altAlgorithm":1,
                    "biggestAngle":153,
                    "waveformSignature":null,
                    "classificationAlg":""
                    }
                ],
            "buddies":null,
            "buddiesTimeStamps":null
            }"""
        #because this is a json object, we can use the same base methods as used in V3
        # first we decode the json string, this puts everything into a dictionary
        jsonDict = json.loads( self.messageString )
        
        try:
            jsonDict.keys()
        except AttributeError:
            # json string is probebly coming from python output, which is slightly wrong
            # do some reformatting for it to work.
            jsonDict = json.loads( self.messageString.strip().replace('\\','').replace('"{','{').replace('}"','}') )

        #apply all the keys as attributes to self
        #this works so easy because the attributes of this object are named the same 
        #as the attributes of the json string (almost), the outliers we handle with 
        #_update_sepcial_attributes
        for key in jsonDict.keys():
            
            value = jsonDict[key]
            key = key.lower()
            if isinstance( value, basestring ):
                #lets see if we can convert this to a number
                try:
                    value = string2number( value )
                except:
                    #I guess we keep it a string
                    pass

            if key == 'pulses':
                #we need to decode the pulses too
                self.pulses = []
                for pdict in jsonDict[key]:
                    messageString = json.dumps( pdict )
                    pulse = Pulse( messageString, version=4, format='rawjson' )
                    self.pulses.append( pulse )
            else:
                setattr( self, key, value )

        #we need to handle a change in format for the bounding box, even though the new format is better
        #for backwards compatibility reasons
        self.maxlatitude  = self.boundingbox[0][1]
        self.minlatitude  = self.boundingbox[0][0]
        self.maxlongitude = self.boundingbox[1][1]
        self.minlongitude = self.boundingbox[1][0]


        #special fields (time)
        self._update_special_attributes( )

    def _decode_oneminute( self ):
        """
        This data format is legacy and not very good.  But it's used by 
        the one-minute data-ingest routine, and the R&D archive.  
        See code for more information
        """
        # header
        #'FlashID,                  Sequential number used by DB
        # FlashGUID,                GUID for flash
        # Lightning_Time,           time object in DB, now just time in different format
        # Lightning_Time_String,    time stamp, accurate to 1ns
        # Latitude,                 float, degrees
        # Longitude,                float, degrees
        # Height,                   int, meters
        # Stroke_Type,              0 = CG, 1 = IC.  Type 40 not explicit
        # Amplitude,                int, amperes
        # Stroke_Solution,          json string where engineering shoves shit that doesn't fit
        # Confidence,               int, 0-100, meaningless
        # LastModifiedTime,         timestamp
        # LastModifiedBy            "LtgInsertFlash_pr"

        # example stroke solution:
        # WWLLN
        # "{"st":"2021-11-17T00:00:00.011499000","et":"2021-11-17T00:00:00.011499000","v":null,"ns":5,"im":0,"cm":1,"aa":9.9253,"ia":9.9253,"ao":120.2022,"io":120.2022,"d":0.0,"s":"wwlln"}"
        # TLN
        # "{"st":"2021-11-17T00:00:00.263354146","et":"2021-11-17T00:00:00.263354146","v":"4.1.0.6","ns":6,"im":1,"cm":0,"aa":-2.07914,"ia":-2.07914,"ao":-49.77624,"io":-49.77624,"d":0.0,"s":"tln"}"
        # "st": start timestamp
        # "et": end timestamp
        # "v":  version (null for wwlln)
        # "ns": number sensors
        # "im": IC Multiplicity
        # "cm": CG multiplicity
        # "aa": max latitude
        # "ia": min latitude
        # "ao": max longitude
        # "io": min longitude
        # "d":  duration in seconds
        # "s":  source ('wwlln' | 'tln' )

        fields = self.messageString.split(',')
        self.flashid   = fields[0]
        self.flashguid = fields[1]
        self.lightningtime = fields[2]
        self.timestamp = fields[3]
        self.latitude  = float( fields[4] )
        self.longitude = float( fields[5] )
        self.altitude  = float( fields[6] )
        self.type      = int( fields[7] )
        self.amplitude = float( fields[8] )
        #stroke solution, how far do we search?
        #we key off the beginning and end of the json string
        self.strokesolution = ','.join( fields[9:-3] )
        self._decode_strokesolution()
        self.confidence = fields[-3]
        self.lastmodifiedtime = fields[-2]
        self.lastmodifiedby   = fields[-1]

        self._update_special_attributes( )

    def _decode_strokesolution( self ):
        ###
        # the stroke solution is a json string used by the engineers to shove data they don't have a field in the DB to put
        # it's stupid
        # right now, this only supports the most recent one-minute file format
        strokeSolution = self.strokesolution.strip().strip('"')
        # 2017/09/15 they switch from | to , deliniated json for some reason
        if '|' in strokeSolution:
            strokeSolution = strokeSolution.replace( '|', ',' )
        
        # prior to 2014/07/01 stroke solution is empty
        if strokeSolution == '':
            return self._generate_null_strokesolution()

        dic = json.loads( strokeSolution )
        # "st": start timestamp
        # "et": end timestamp
        # "v":  version (null for wwlln)
        # "ns": number sensors
        # "im": IC Multiplicity
        # "cm": CG multiplicity
        # "aa": max latitude
        # "ia": min latitude
        # "ao": max longitude
        # "io": min longitude
        # "d":  duration in seconds
        # "s":  source ('wwlln' | 'tln' )
        headerConversion = {
            "st":"starttimestamp",
            "et":"endtimestamp",
            "v" :"version",
            "ns":"numsensors",
            "im":"icmultiplicity",
            "cm":"cgmultiplicity",
            "aa":"ullatitude",  #max lat on the top
            "ia":"lrlatitude",  #min lat on the bottom
            "ao":"lrlongitude", #max lon on the right
            "io":"ullongitude", #min lon on the left
            "d": "duration",
            "s": "source",
        }
        for key in dic:
            if key in headerConversion:
                setattr( self, headerConversion[key], dic[key] )
        #units conversion
        #duration is spec'd in seconds for some reason
        if hasattr( self, 'duration' ):
            self.duration = int( self.duration* 1000000000 )
        #check for wwlln
        if hasattr( self, "source" ):
            #for backfill data, it looks like this is populated null
            if self.source is not None:
                if 'wln' in self.source or 'wwlln' in self.source:
                    self.type = 40
            elif hasattr( self, 'version'):
                #fall back on other methods
                if self.version is None:
                    self.type = 40

    def _generate_null_strokesolution( self ):
        """Generates fill values for the things in stroke solution in the case one doesn't exist
        """
        # "st": start timestamp
        # "et": end timestamp
        # "v":  version (null for wwlln)
        # "ns": number sensors
        # "im": IC Multiplicity
        # "cm": CG multiplicity
        # "aa": max latitude
        # "ia": min latitude
        # "ao": max longitude
        # "io": min longitude
        # "d":  duration in seconds
        # "s":  source ('wwlln' | 'tln' )

        self.starttimestamp = self.timestamp
        self.endtimestamp   = self.timestamp
        self.duration = 0
        self.numsensors = 0
        self.icmultiplicity = 0
        self.cgmultiplicity = 0
        self.ullatitude = self.latitude
        self.lrlatitude = self.latitude
        self.ullongitude = self.longitude
        self.lrlongitude = self.longitude
        self.source = 'tln'

    def _update_special_attributes( self ):
        '''
        Updates a small number of special fields which don't come directly 
        from the feed message string
        '''
        for key in self._altKeys:
            if hasattr( self, key ):
                #set the standardized version
                setattr( self, self._altKeys[key], getattr( self, key) )
        #json peculiarities:
        #height, is named 'icheight'
        if hasattr( self, 'icheight' ):
            self.height = self.icheight

        #convert timestamp to epoch and nano, or vice versa (depending on binary or ascii)
        if self.epoch is None:
            #try generating from time stamp, 
            #this happens in ascii feeds
            self.epoch, self.nano = timestamp2epochnano( self.timestamp )
            self.time = self.epoch + self.nano/1.e9
        elif self.timestamp is None:
            #generate the timestamp from the epoch and nano
            self.timestamp = epoch2timestamp( self.epoch ) + '.%09i'%self.nano
    
        #start time
        if self.starttimestamp is not None:
            #try generating from time stamp, 
            #this happens in most ascii based formats
            self.startepoch, self.startnano = timestamp2epochnano( self.starttimestamp )
            self.starttime = self.startepoch + self.startnano/1.e9
        elif self.startepoch is not None and self.startnano is not None:
            #we can also generate of the epoch and nano values, this happens in binary formats
            self.starttimestamp = epoch2timestamp( self.startepoch ) + '.%09i'%self.startnano
            self.starttime = self.startepoch + self.startnano/1.e9
        elif self.starttime is not None:
            #used in state files, how did we get here
            #we can generate off the start time, but it's not as accurate
            self.startepoch = int( self.starttime )
            self.startnano  = int( 1000000000 * (self.starttime-self.startepoch) )
            self.starttimestamp = epoch2timestamp( self.startepoch ) + '.%09i'%self.startnano
        else:
            #we have no starttime information, generate a default
            self.starttimestamp = self.timestamp
            self.startepoch, self.startnano = timestamp2epochnano( self.starttimestamp )
            self.starttime = self.startepoch + self.startnano/1.e9
        
        #end time, all the same shit as above, one more time but a little different
        if self.endtimestamp is not None:
            #easy case, convert
            self.endepoch, self.endnano = timestamp2epochnano( self.endtimestamp )
            self.endtime = self.endepoch + self.endnano/1.e9
        elif self.endepoch is not None and self.endnano is not None:
            #also easy case, convert, this may happen in binary formats, or maybe not
            self.endtimestamp = epoch2timestamp( self.endepoch ) + '.%09i'%self.endnano
            self.endtime = self.endepoch + self.endnano/1.e9
        elif self.endtime is not None:
            self.endepoch = int( self.endtime )
            self.endnano  = int( 1000000000 * (self.endtime-self.endepoch) )
            self.endtimestamp = epoch2timestamp( self.endepoch ) + '.%09i'%self.endnano
        else:
            #maybe we can regenerate from starttime and duration?
            self.endepoch = self.startepoch
            self.endnano  = self.startnano + self.duration
            if self.endnano > 1000000000:
                self.endepoch += int( self.endnano//1000000000 )
                self.endnano %= 1000000000
            self.endtime = self.endepoch + self.endnano/1.e9
            self.endtimestamp = epoch2timestamp( self.endepoch ) + '.%09i'%self.endnano


        #V2 multiplicity to IC and CG multiplicity
        #ascii format gives only total multiplicity, not individual values
        if self.multiplicity is not None:
            if self.type == 1:
                self.icmultiplicity = self.multiplicity
                self.cgmultiplicity = 0
            else:
                self.cgmultiplicity = self.multiplicity
                self.icmultiplicity = 0

        ################################################
        # Fill Values
        ###
        #bounding box stuff
        if self.ullatitude is None: self.ullatitude = self.latitude
        if self.lrlatitude is None: self.lrlatitude = self.latitude 
        if self.ullongitude is None: self.ullongitude = self.longitude
        if self.lrlongitude is None: self.lrlongitude = self.longitude

        for key in self._altKeys:
            if hasattr( self, key ):
                #set the standardized version
                setattr( self, self._altKeys[key], getattr( self, key) )

    def add_pulse( self, pulse, update=False, v2=False ):
        #adds a pulse to the list of pulses
        self.pulses.append( pulse )
        
        if update:
            self._update_from_pulses( v2 )
            
    def _update_from_pulses( self, v2 ):
        '''
        Update the relevant flash fields from the pulses
        '''
        if len(self.pulses) == 1:
            # This is the first pulse in the flash, fill in all possible values for a flash
            pulse = self.pulses[0]
            self.type      = pulse.type   #0 CG, 1 IC, 40 WWLLN
            self.latitude  = pulse.latitude   #in decimal degrees
            self.longitude = pulse.longitude   #in decimal degrees
            self.timestamp = pulse.timestamp   #string YYY-MM-DDTHH:MM:SS.nnnnnnnnn (except v2)
            self.starttimestamp=pulse.timestamp #Time of the first pulse in the flash
            self.starttime      = pulse.time   #unix epoch time - float number of seconds since 1970. Only accurate to microseconds
            self.endtime        = pulse.time   #unix epoch time - float number of seconds since 1970. Only accurate to microseconds
            self.startepoch     = pulse.epoch  #unix epoch time - integer number of seconds since 1970
            self.startnano      = pulse.nano   #integer number of nanoseconds after the epoch 
            self.amplitude = pulse.amplitude   #in Amps
            self.height    = pulse.altitude   #in meters above sealevel
            self.numsensors=pulse.numsensors   #the number of sensors which contributed to the solution
            self.multiplicity=None  #icMultiplicity + cgMultiplicity
            if pulse.type==1:
                self.icmultiplicity=1 #number of IC pulses included in this flash
                self.cgmultiplicity=0 #number of CG pulses included in this flash
            else:
                self.icmultiplicity=0 #number of IC pulses included in this flash
                self.cgmultiplicity=1 #number of CG pulses included in this flash
            self.maximumlatitude= pulse.latitude   #upper left (max) latitude
            self.minimumlatitude= pulse.latitude   #lower right (min) latitude
            self.minimumlongitude=pulse.longitude   #upper left (min) longitude
            self.maximumlongitude=pulse.longitude   #lower right (max) longitude
            self.duration  = 0   #duration of the flash in nanoseconds
        else:
            pulse = self.pulses[-1]
            # If pulse is largest, Update peak current, time, location, and height
            if np.abs(self.peakcurrent) < np.abs(pulse.amplitude):
                self.amplitude = pulse.amplitude
                self.starttime = pulse.time
                self.timestamp = pulse.timestamp
                self.startepoch= pulse.epoch
                self.startnano = pulse.nano
                self.latitude = pulse.latitude
                self.longitude = pulse.longitude
                self.height = pulse.altitude
            
            # Update startTime, endTime, and duration
            if self.starttime > pulse.time:
                self.starttimestamp=pulse.timestamp
                self.starttime = pulse.time
                self.duration = int((self.endtime - self.starttime)*1e9)
            elif self.endtime < pulse.time:
                self.endtime = pulse.time
                self.duration = int((self.endtime - self.starttime)*1e9)
            
            # Update bounding box
            if self.minimumlatitude > pulse.latitude:
                self.minimumlatitude = pulse.latitude
            if self.maximumlatitude < pulse.latitude:
                self.maximumlatitude = pulse.latitude
                
            if self.minimumlongitude > pulse.longitude:
                self.minimumlongitude = pulse.longitude
            if self.maximumlongitude < pulse.longitude:
                self.maximumlatitude = pulse.longitude
            
            # Update type and multiplicity
            if pulse.type == 1:
                self.icmultiplicity += 1
            elif pulse.type == 0:
                self.type = 0
                self.cgmultiplicity += 1
            elif pulse.type == 40:
                self.type = 40
                self.cgmultiplicity += 1
            
        self._update_special_attributes( v2 )

    def printout(self):
        S = ''
        S += ('%s,' %self.timestamp).rjust(22)
        S += (' %2i,'   %self.type           ).rjust(4)
        S += (' %5.4f,' %self.latitude       ).rjust(12)
        S += (' %5.4f,' %self.longitude      ).rjust(12)
        S += (' %9i,'   %self.peakcurrent    ).rjust(11)
        S += (' %s,'    %repr(self.cgmultiplicity) ).rjust(5)
        S += (' %s,'    %repr(self.icmultiplicity) ).ljust(5)
        S += (' %i'     %len(self.pulses) )
        return S

