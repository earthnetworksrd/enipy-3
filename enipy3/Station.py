###
# Python 2/3 compatibility stuff
from __future__ import absolute_import
from __future__ import print_function
from __future__ import division
from __future__ import unicode_literals
from io import IOBase, open   #needed for python 3 file instance checking
#needed for string instance checking
try: basestring
except NameError: 
    basestring = (str, bytes)
import numpy as np

class LocFile( ):
    def __init__(self, filelike):
        
        #build the catalog
        self.read(filelike)
    
    def read(self, filelike=None):
        #open the input file for reading
        if isinstance( filelike, basestring ):
            #it's a string, try and open
            self.f = open( filelike, 'r' )
        elif isinstance( filelike, IOBase ):
            #then it's a file
            self.f = filelike
            
        self.stationID = list()
        self.stationLat = list()
        self.stationLon = list()
        self.stationAlt = list()
        self.stations = {}
        
        lines = self.f.readlines()
        for l in lines:
            l = l.strip().split()
            #skip expty lines
            if l == '':
                continue
            stationID = l[0]
            stationLat = float(l[1])
            stationLon = float(l[2])
            if len(l) > 3:
                stationAlt = float(l[2])
            else:
                stationAlt = 0
            
            #expand the arrays and append
            self.stationID.append(stationID)
            self.stationLat.append(stationLat)
            self.stationLon.append(stationLon)
            self.stationAlt.append(stationAlt)

            #and the dictionary
            self.stations[stationID] = stationLat, stationLon
            
        # Convert from list to arrays
        self.stationID = np.asarray(self.stationID)
        self.stationLat = np.asarray(self.stationLat)
        self.stationLon = np.asarray(self.stationLon)
        self.stationAlt = np.asarray(self.stationAlt)
                        
    def close(self):
        self.f.close()	



class Station ( ):
    '''
    Class that holds information about a single sensor,
    Included in here are some numbers to help with determining the 'quality' of the 
    sensor (threshold and signal).  These are used to determine which sensors to 
    look at for each cell.

    Parameters
    ----------
    lat : int
        latitiude
    lon : int
        longitude
    alt : int
        altitude,
    gpsVisibility : boolean
        Value for settings the value for stations tracking satellites

    Returns
    -------
    None
        This class has only a single constructor object
    '''
    def __init__( self, ID, lat, lon, alt=0, gpsVisibility=True):
        self.ID  = ID
        self.lat = lat
        self.lon = lon
        self.alt = alt
        self.gpsVisibility = gpsVisibility
        self.pt  = lat,lon,alt #this is convenient for passing the location to a distance function

