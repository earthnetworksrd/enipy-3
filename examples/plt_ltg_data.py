####
# A short example on how to load an LTG file, and plot some data
import enipy as lx
import matplotlib.pyplot as plt
import os, sys

#######################################################################
# These Values will need to be modified
# look for data near this lat,lon
referenceLocation  = 0, 0
# look for data at this time (to the second)
referenceTimeStamp = '2018/11/10T20:00:00'
# how far should we search?
searchDistance = 1500  #km
# change this to make the waveforms bigger or smaller in amplitude
amplitudeNormalization = 100.
# Read from this file
inputFile = '/data2/LTG/2020/08/26/Archive-2020-08-26T10-20.LTG'
inputFile = '<path to file>'



###
# Step 1, open the LTG file
if not os.path.exists( inputFile ):
    print ("ERROR - input LTG file path appears to be missing: %s"%inputFile )
    sys.exit()
ltgFile = lx.Ltg.LtgFile( inputFile )
#we want to know the locations of all the waveforms from at timestamp
#to do this, we're going to need to convert the timestamp to epoch format
epoch,nano = lx.timeStamp2epochnano( referenceTimeStamp )
#getting the file locations is easy
fileLocations = ltgFile.fileLocationsByEpoch[ epoch ]
#loop over the file locations, and seach for data within X km from the reference location
waveforms = []
for l in fileLocations:
    #check the message type
    if ltgFile.catalog[l].version[0] != 1:
        #this is not a waveform packet, ignore it
        continue
    ltgPacket = ltgFile.read( l )

    #does this packet tell us where it is?
    #the older version packets don't, for those you have to look up the 
    #location in a table.  
    #we can also use that location table to filter without decoding 
    #the packets, which is faster
    if ltgPacket.lat is None or ltgPacket.lon is None:
        continue

    #is this packet within the search distance?
    distance = lx.oblate_distance( referenceLocation, [ltgPacket.lat,ltgPacket.lon] )
    if distance/1000. > searchDistance:
        #no it isn't, move on
        continue

    #add it to our list of waveforms
    waveforms.append( ltgPacket )

###
# now, lets plot the waveforms
fig, ax = plt.subplots( 1,1 )
for ltgPacket in waveforms:
    distance = lx.oblate_distance( referenceLocation, [ltgPacket.lat,ltgPacket.lon] )
    offset = distance/1000
    #ltg.packet.hf is a Nx2 array of time,amplitude pairs
    #time is in nanoseconds from the second
    #amplitude is in raw digitizer values
    #plot lines
    plt.plot( ltgPacket.hf[:,0]/1000000., ltgPacket.hf[:,1]/amplitudeNormalization + offset, '-' )
    #plot samples
    plt.plot( ltgPacket.hf[:,0]/1000000., ltgPacket.hf[:,1]/amplitudeNormalization + offset, 'k,' )

plt.xlabel( 'Time since %s [ms]'%(lx.epoch2timeStamp(epoch)) )
plt.ylabel( 'Distance from %s [km]'%(repr(referenceLocation)) )
plt.show()