import time, calendar
import numpy as np
from .constants import *

def string2number( value ):
    if '.' in value:
        return float( value )
    else:
        return int( value )

def median( arr ):
    #don't sort in place, python has pointer
    arr = sorted( arr )
    return arr[ len(arr)//2 ]

def quartile( arr ):
    #don't sort in place
    arr = sorted( arr )
    return arr[ len(arr)//4 ], arr[ 3*len(arr)//4 ]

def pythagorean_distance( pt1, pt2 ):
    '''
    This method is used for finding
    the pythagorean distance

    the pythagorean approximation is very fast, but has large error if 
    the points are farther than about 100 km apart

    Arguments
    ---------
    :param pt1: lat, lon location
    :param pt2: lat, lon location

    Returns
    -------
    :return: Calculated distance value
    '''
    #math imports
    from numpy import cos, sin, arctan2, pi, sqrt
    
    #copy and convert to radians
    lat1 = pt1[0]*pi/180
    lat2 = pt2[0]*pi/180
    lon1 = pt1[1]*pi/180
    lon2 = pt2[1]*pi/180    

    F = (lat1+lat2)/2
    G = (lat1-lat2)/2
    L = (lon1-lon2)/2
    
    #use pythagorean approximation, it's less accurate but good enough

    KPD_lat = 111.13209-0.56605*cos(2*F)+0.0012*cos(4*F)
    KPD_lon = 111.41513*cos(F)-0.09455*cos(3*F)+0.00012*cos(5*F)

    rng = np.sqrt( (KPD_lon*(lon1-lon2)*180/np.pi)**2 + (KPD_lat*(lat1-lat2)*180/np.pi)**2 )
    return rng*1000

def oblate_distance( pt1, pt2, ):
    '''
    This method is used for finding the distance
    from the two points

    uses an oblate spheroid model of the earth.  Slower than pythagorean, 
    but very accurate even at extremely long distances.  
    Automatically reverts to pythagorean calculation at short distances

    Parameters
    ---------
    :param pt1: lat, lon location
    :param pt2: lat, lon location

    Returns
    -------
    :return: Calculated distance value
    '''
    #math imports
    from numpy import cos, sin, arctan2, pi, sqrt

    #copy and convert to radians
    lat1 = pt1[0]*pi/180
    lat2 = pt2[0]*pi/180
    lon1 = pt1[1]*pi/180
    lon2 = pt2[1]*pi/180    

    F = (lat1+lat2)/2
    G = (lat1-lat2)/2
    L = (lon1-lon2)/2
    
    #for very small distances, we run into a divide by 0 error
    #so for these, we switch over to a different algorithm which executes a little faster
    if L**2+G**2 < 0.000001:
        # print ('pythag', L, G )
        #use pythagorean approximation, it's less accurate but good enough

        KPD_lat = 111.13209-0.56605*cos(2*F)+0.0012*cos(4*F)
        KPD_lon = 111.41513*cos(F)-0.09455*cos(3*F)+0.00012*cos(5*F)

        rng = np.sqrt( (KPD_lon*(lon1-lon2)*180/np.pi)**2 + (KPD_lat*(lat1-lat2)*180/np.pi)**2 )
        return rng*1000

    #trig functions take computation time, calculate them all at once, and only once
    sing = sin(G)
    cosl = cos(L)
    cosf = cos(F)
    sinl = sin(L)
    sinf = sin(F)
    cosg = cos(G)

    S = sing*sing*cosl*cosl + cosf*cosf*sinl*sinl
    C = cosg*cosg*cosl*cosl + sinf*sinf*sinl*sinl
    W = arctan2(sqrt(S),sqrt(C))

    R = sqrt((S*C))/W
    H1 = (3 * R - 1.0) / (2.0 * C)
    H2 = (3 * R + 1.0) / (2.0 * S)
    D = 2 * W * R_EARTH_EQUA
    return (D * (1 + EARTH_FLATTENING * H1 * sinf*sinf*cosg*cosg - EARTH_FLATTENING*H2*cosf*cosf*sing*sing))

def interior_angle( A1, A2 ):
    #calculates the interior angle of A1 and A2
    #due to the way % works on floats, this will always be positive
    return min( [ (A1-A2)%360., (A2-A1)%360.  ])

def spherical_bearing( pt1, pt2 ):
    '''
    This method is used to Calculate the forward azimuth between pt1 and pt2.  You can
    get the back azimuth by calling on pt2, pt1.

    Parameters
    ----------
    :param pt1: starting position (lat,lon)
    :param pt2: ending position (lat,lon)

    Returns
    -------
    :return: Calculated Spherical bearing in degrees
    '''
    #mat imports
    from numpy import cos, sin, arctan2, pi

    #copy and convert to radians
    lat1 = pt1[0]*pi/180
    lat2 = pt2[0]*pi/180
    lon1 = pt1[1]*pi/180
    lon2 = pt2[1]*pi/180    
    
    x = sin(lon2-lon1)*cos(lat2)
    y = cos(lat1)*sin(lat2) - sin(lat1)*cos(lat2)*cos(lon2-lon1)
    return np.arctan2( x, y )*180/np.pi

def rngbrg2latlon( rng, brg, pt ):
    """converts range and bearing from a lat-lon point to a lat-lon point
    Based on a spherical model of the earth, so results are approximate
    """
    ###
    # gonna need these.  Maybe they've already been imported, but 
    # it won't hurt to do it again
    from numpy import arcsin, arctan2, sin, cos, pi
    
    lat1 = pt[0]*pi/180
    lon1 = pt[1]*pi/180
    
    d = rng/R_EARTH
    
    lat2 = arcsin( sin(lat1)*cos(d) + cos(lat1)*sin(d)*cos(brg) )
    lon2 = lon1 + arctan2( sin(brg)*sin(d)*cos(lat1), cos(d)-sin(lat1)*sin(lat2) )
    
    #lat and lon should be returned in degrees
    return lat2*180/pi, lon2*180/pi

########################################################################
# Time Functions   
def epoch2timestamp( t ):
    '''
    This method is used to convert the epoch to the Time stamp

    Parameters
    ----------
    :param t: Time value

    Returns
    -------
    :return: Time stamp value
    '''
    S = time.strftime( '%Y-%m-%dT%H:%M:%S', time.gmtime( t ) )
    return S

def timestamp2epochnano( timeStamp ):
    '''
    This method converts a timeStamp is a reasonable YearMonthDayHourMinuteSecond format into
    linux time (Seconds since 1970)
    Does this in a fairly flexible manner, maybe more so than needed

    Parameters
    ----------
    :param timeStamp: Timstamp String value


    Returns
    -------
    :returns Epoch seconds and epoch nano seconds
    '''
    #strip all non-numbers from the timeStamp
    strippedString = ''
    for c in timeStamp:
        if c.isdigit():
            strippedString += c

    #the first 14 numbers are for the date and time, the second 9 are the for fractional seconds
    if len(strippedString) < 14+9:
        #fill with 0's
        strippedString += '0'*(14+9-len(strippedString))

    #return epoch seconds, and nano seconds back
    return calendar.timegm( time.strptime( strippedString[:14], '%Y%m%d%H%M%S' ) ), int( strippedString[14:] )

########################################################################
# Python 3 idiocy functions
# the move to python 3 means that strings are sometimes not strings, and
# things are done with special classes which could be done with normal 
# base classes.  Sometimes extra helper functions are needed which 
# weren't needed before.  All in the name of 'progress' or something
def string( S ):
    #python3 now has bytes and unicode.  
    #text pulled from normal ascii is now automatically converted to unicode
    #text coming from binary data stays 1byte ascii
    #the types are incompatible with each other, True state don't work, concatinate doens't work, etc
    #here, we unify around unicode strings
    if isinstance( S, bytes ):
        return S.decode()
    else:
        return S